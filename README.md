# Introduction 
SiLA2 .NET 6 implementation (https://sila-standard.com/)  [![pipeline status](https://gitlab.com/Chamundi/SiLA2-grpc-dotnet/badges/main/pipeline.svg)](https://gitlab.com/Chamundi/SiLA2-grpc-dotnet/-/commits/main)
  * Migration from https://gitlab.com/SiLA2/sila_csharp which depends on deprecating native gRPC-Implementation
  * No renamed native libraries compiled for ARM32 necessary anymore
  * SiLA2.Server even runs on embedded Linux Host as recommended AspNetCore-Process
  * Feature-To-Proto-Generation by XSLT (no JAVA Runtime nor Sila.Tools needed anymore)
    * SiLA2 Core Features included in SiLA2.dll
    * Additional Features should be part of Feature-Implementation Assemblies (just add your features and protos in MSBuild Targets ProtoPreparation & ProtoGeneration like it is done in Assemblies TemperatureController.Features.csproj or ShakerController.Features.csproj)
  * Extensible InProcess Server Web Frontend included (based on Blazor (https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor) which supports "server push" functionality)

   ![image](cdn_read_me/rtmonexample.jpg) 

  * Optional InProcess-Database-Module (SQLite) with basic User Management which can be easily extended...for example as analytic data storage...
  * Optional Inter Process Communication Module (SiLA2.IPC.NetMQ) for Socket Communication...compatible with ZeroMQ (https://zeromq.org/)...
   
# Prerequisites
  * Linux / macOS
    * You´ll need the .NET 6 SDK >> https://dotnet.microsoft.com/download/dotnet/6.0
	* It´s not necessary to build applications with a GUI but if you do so an IDE like Visual Studio Code ( >> https://code.visualstudio.com/ ) would be convenient
  * Windows
    * Download free IDE Visual Studio 2022 Community ( >> https://visualstudio.microsoft.com/de/vs/community/ ), use commercial Visual Studio 2022 Version or Visual Studio Code as well
	* .NET 6 SDK is included in Visual Studio 2022...if you want to use Visual Studio Code or other IDEs you´ll have to download it on your own (see link above)

# Getting Started
  * Clone Repo
    * Please be sure fetching sila_base submodule by
      * checking out the Repository with git-submodules recursively 
        * git clone --recurse-submodules https://gitlab.com/Chamundi/SiLA2-grpc-dotnet.git
      * or check out the Repository and run following commands
        * git submodule init
        * git submodule update
  * Run gRPC-Server
    * SiLA2.gRPC.Temperature.Service.Basic
    or
    * SiLA2.gRPC.Temperature.Service (containing optional WebFrontend- and DatabaseModule)
      * After having started the SilaServer process you can also follow Link https://server-hostname-or-server-ip:13742 (in Debug-Mode https://localhost:5001) to open a SilaServer-WebFrontend
      * In the SilaServer-WebFrontend you´ll find NavigationLink "User Management"-View to use SilaServer-Database. There´s also an example of how Server-Push-Feature can be used...just click on NavigationLink "Temperature" and hit button "Change Temperature"...
  * Run SiLA2.Temperature.Service.Client.App connecting automatically to SilaServer

# Build your own Project based on official Nuget-Packages
  * Created ASP.NET Core Application as SiLA2.Server Project
  * Search for & reference SiLA2.* packages found at Nuget.org in Visual Studio or https://www.nuget.org/ ...use at least SiLA2.Core...
  * Create *.sila.xml-Feature-File and include it like it was done in Example Project SiLA2.Referencing.Nuget.Features.csproj
  * Implement the features you´ve defined in your *.sila.xml-Feature-File in your Feature-Assembly
  * Reference your Feature-Assenbly in your SiLA2.Server Project
  * Create SilA2-Clients communicating with the SiLA2.Server...in this case you might want to use Nuget-Package SiLA2.Client... 

# Build and Test
* Just build Solution and run as described in "# Getting Started". For a first test you could use the SiLA 2 Browser by UniteLabs (https://sila-standard.com/dipitems/sila-2-browser/)...
* ...or use the SiLA Universal Client (https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client)...it´s still experimental and it could be possible that you have to register SiLA2 Servers manually by IP yet...
* If you have problems building the solution you might clean your Nuget-Cache by 'rmdir -r %UserProfile%\.nuget\packages\*' once...

# Contribute
It´s Open Source (License >> MIT)...feel free to use or contribute. For Merge-Requests contact me by E-Mail >>> CPohl@inheco.com or Stefan Koch >>> koch@equicon.de

# Open Questions
( >> https://gitlab.com/Chamundi/SiLA2-grpc-dotnet/-/issues )

 
