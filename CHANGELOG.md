# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and starting with version number 1.0.0 this project will adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The version numbers in this changelog refer to all SiLA2 assemblies and their nuget packages (guaranteed to match).

## [vNext]

## SiLA2.Server.Database [6.0.2] -> [6.0.3]
- BREAKING CHANGE:  Changed DataType of generic BaseEntity Key from INT to GUID

## SiLA2.Server [6.0.6] -> SiLA2.Core [6.0.7]
- BREAKING CHANGE:  Changed Nuget-Package-Name of SiLA2.csproj from 'SiLA2.Server' to 'SiLA2.Core'

### Incompatible Changes 
- Moved IGrpcChannelProvider & GrpcChannelProvider to SiLA2.Utils
- Created IGrpcServerChannelProvider & GrpcServerChannelProvider for serverside channel creation in SiLA2
- Deleted NetworkService & renamed INetworker & Networker to NetworkService in SiLA2.Utils

### Compatible Changes
- Included Binary-Upload and -Download implementation in SiLA2
- NetworkService.GetNetworkInterfaceByName looks for an active interface by name. If it is not found it will take the first active NetworkInterface or throw an NetworkInterfaceNotFoundException in SiLA2.Utils


## [6.0.0] - (everything before introduction of the changelog)
- First Release with SiLA2.Server.nupkg and SiLA2.Utils.nupkg on nuget.org
