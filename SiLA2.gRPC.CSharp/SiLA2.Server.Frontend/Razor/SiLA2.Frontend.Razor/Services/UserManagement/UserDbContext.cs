﻿using DbUp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SiLA2.Frontend.Razor.Services.UserManagement.Maps;
using SiLA2.Server.Database;
using SiLA2.Server.Database.Domain;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public class UserDbContext : DbContext, IDbUserContext
    {
        private readonly ILogger<UserDbContext> _logger;

        public IEnumerable<Type> DbMappingTypes => new Type[] { typeof(UserMap) };

        public UserDbContext(DbContextOptions<UserDbContext> options, IConfiguration configuration, ILogger<UserDbContext> logger) : base(options)
        {
            _logger = logger;

            try
            {
                if (Database.EnsureCreated())
                {
                    var deploymentResult = DeployChanges.To.SQLiteDatabase(configuration["ConnectionStrings:DefaultConnection"]).WithScriptsEmbeddedInAssemblies(new[] { Assembly.GetAssembly(typeof(UserDbContext)) }).Build().PerformUpgrade();
                    _logger.LogInformation($"Database {nameof(UserDbContext)} Deployment succeeded : {deploymentResult.Successful}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating Database");
            }
        }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var type in DbMappingTypes)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}
