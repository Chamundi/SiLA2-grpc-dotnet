﻿using System;

namespace SiLA2.Frontend.Razor.Services.UserManagement.Domain
{
    [Flags]
    public enum Role
    {
        Admin,
        Standard,
        Undefined
    }
}
