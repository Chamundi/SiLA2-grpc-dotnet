﻿using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using SiLA2.Server.Database;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(IDbUserContext context) : base(context) { }
    }
}