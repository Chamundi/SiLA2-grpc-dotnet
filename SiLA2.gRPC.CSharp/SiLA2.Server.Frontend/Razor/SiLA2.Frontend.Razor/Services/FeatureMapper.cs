﻿using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.Frontend.Razor.ViewModels;
using SiLA2.Server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services
{
    public class FeatureMapper : IFeatureMapper
    {
        private static string SILA2_SERVICE_CLIENT_KEY = "sila2.org.silastandard.core.silaservice.v1.SiLAService";
        private static string SILA2_ORG_SILASTANDARD_NAMESPACE_PREFIX = "sila2.org.silastandard";

        private readonly IGrpcClientProvider _grpcClientProvider;

        public FeatureMapper(IGrpcClientProvider grpcClientProvider)
        {
            _grpcClientProvider = grpcClientProvider;
        }

        public async Task<List<FeatureItemViewModel>> GetViewModels()
        {
            await _grpcClientProvider.CreateClients();

            var features = await GetImplementedFeatures();

            var featureViewItemList = new List<FeatureItemViewModel>();
            foreach (var feature in features)
            {
                var featureItemsViewModel = new FeatureItemViewModel(feature.DisplayName, feature.FullyQualifiedIdentifier, feature.Description, feature.Namespace, feature.Identifier);

                foreach (var item in feature.Items)
                {
                    if (item.GetType() == typeof(FeatureProperty))
                        featureItemsViewModel.FeatureProperties.Add((FeatureProperty)item);
                    else if (item.GetType() == typeof(FeatureCommand))
                        featureItemsViewModel.FeatureCommands.Add((FeatureCommand)item);
                    else if (item.GetType() == typeof(FeatureMetadata))
                        featureItemsViewModel.FeatureMetadata.Add((FeatureMetadata)item);
                    else if (item.GetType() == typeof(FeatureDefinedExecutionError))
                        featureItemsViewModel.FeatureDefinedExecutionErrors.Add((FeatureDefinedExecutionError)item);
                    else if (item.GetType() == typeof(SiLAElement))
                        featureItemsViewModel.SilaElements.Add((SiLAElement)item);
                    else
                        throw new NotImplementedException($"Unknown FeatureItem -> {item}");
                }

                if (_grpcClientProvider.ClientMap.ContainsKey(featureItemsViewModel.TypeIdentifier))
                {
                    var methodInfos = await _grpcClientProvider.GetMethodInfosAsync(featureItemsViewModel.TypeIdentifier);

                    foreach (var item in methodInfos)
                    {
                        var featureMethodViewModel = new FeatureMethodInfoViewModel(item);
                        featureItemsViewModel.Methods.Add(featureMethodViewModel);
                        var parameters = item.GetParameters().Where(x => x.ParameterType.FullName.ToLower().StartsWith(feature.Namespace));
                        foreach (var p in parameters)
                        {
                            var parameterBundle = new Tuple<ParameterInfo, IList<PropertyInfoViewModel>>(p, new List<PropertyInfoViewModel>());
                            var silaProperties = p.ParameterType.GetProperties().Where(y => y.PropertyType.FullName.ToLower().StartsWith(feature.Namespace) || y.PropertyType.FullName.ToLower().StartsWith(SILA2_ORG_SILASTANDARD_NAMESPACE_PREFIX));
                            foreach (var parameterProps in silaProperties)
                            {
                                parameterBundle.Item2.Add(new PropertyInfoViewModel(parameterProps));
                            }
                            featureMethodViewModel.Parameters.Add(parameterBundle);
                        }
                    }
                }
                featureViewItemList.Add(featureItemsViewModel);
            }

            return featureViewItemList;
        }

        private async Task<IEnumerable<Feature>> GetImplementedFeatures()
        {
            var sila2ServiceClient = _grpcClientProvider.ClientMap[SILA2_SERVICE_CLIENT_KEY] as SiLAService.SiLAServiceClient;
            var features = await sila2ServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());
            var result = new List<Feature>();
            features.ImplementedFeatures.ToList().ForEach(feature => result.Add(GetFeatureDefinition(sila2ServiceClient, feature.Value)));
            return result;
        }

        private Feature GetFeatureDefinition(SiLAService.SiLAServiceClient silaService, string featureIdentifier)
        {
            GetFeatureDefinition_Responses response;
            response = silaService.GetFeatureDefinition(new GetFeatureDefinition_Parameters
            {
                FeatureIdentifier = new Sila2.Org.Silastandard.String
                {
                    Value = featureIdentifier
                }
            });
            return FeatureGenerator.ReadFeatureFromXml(response.FeatureDefinition.Value);
        }
    }
}
