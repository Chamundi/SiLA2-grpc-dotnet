﻿using SiLA2.Frontend.Razor.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services
{
    public interface IFeatureMapper
    {
        Task<List<FeatureItemViewModel>> GetViewModels();
    }
}