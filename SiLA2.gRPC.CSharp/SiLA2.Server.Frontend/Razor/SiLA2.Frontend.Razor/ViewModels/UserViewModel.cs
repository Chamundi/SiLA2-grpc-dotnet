﻿using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using System;

namespace SiLA2.Frontend.Razor.ViewModels
{
    public class UserViewModel : ICloneable
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public Role Role { get; set; } = Role.Undefined;
        public string Password { get; set; }

        public UserViewModel(Guid id, string login, Role role)
        {
            Id = id;
            Login = login;
            Role = role;
        }

        public UserViewModel() {  }

        public object Clone()
        {
            return new UserViewModel(Id, Login, Role) { Password = Password};
        }
    }
}
