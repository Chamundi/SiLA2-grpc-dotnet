﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace SiLA2.Frontend.Razor.ViewModels
{
    public class FeatureMethodInfoViewModel 
    {
        const string SILANS = "sila2.org.silastandard.";

        public MethodInfo MethodInfo { get; }

        public object Result { get; set; }

        public IList<Tuple<ParameterInfo, IList<PropertyInfoViewModel>>> Parameters { get; } = new List<Tuple<ParameterInfo, IList<PropertyInfoViewModel>>>();

        public FeatureMethodInfoViewModel(MethodInfo methodInfo)
        {
            MethodInfo = methodInfo;
        }
    }
}
