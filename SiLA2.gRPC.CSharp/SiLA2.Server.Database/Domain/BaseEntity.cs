﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SiLA2.Server.Database.Domain
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
