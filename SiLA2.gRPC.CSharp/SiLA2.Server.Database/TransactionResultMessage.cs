﻿namespace SiLA2.Server.Database
{
    public class TransactionResultMessage
    {
        public TransactionResult TransactionResult { get; }
        public string Message { get; }

        public TransactionResultMessage(TransactionResult transactionResult, string message = null)
        {
            TransactionResult = transactionResult;
            Message = message;
        }
    }
}
