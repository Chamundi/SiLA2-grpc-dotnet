﻿using SiLA2.Server.Database.Domain;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Server.Database
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> GetById(object id);
        Task<TransactionResultMessage> Insert(T entity);
        Task<TransactionResultMessage> Update(T entity);
        Task<TransactionResultMessage> Delete(T entity);
        IQueryable<T> Table { get; }
    }
}