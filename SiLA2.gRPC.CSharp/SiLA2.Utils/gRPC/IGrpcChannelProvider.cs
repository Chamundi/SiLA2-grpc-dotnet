﻿using Grpc.Net.Client;
using System.Threading.Tasks;

namespace SiLA2.Utils.gRPC
{
    public interface IGrpcChannelProvider
    {
        Task<GrpcChannel> GetChannel(string host, int port, bool noCertificateValidation = false, GrpcChannelOptions channelOptions = null);
    }
}