﻿using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Net.Http;
using System.Threading.Tasks;

namespace SiLA2.Utils.gRPC
{
    public class GrpcChannelProvider : IGrpcChannelProvider
    {
        private readonly ILogger<GrpcChannelProvider> _logger;

        private ConcurrentDictionary<string, GrpcChannel> _channelMap = new ConcurrentDictionary<string, GrpcChannel>();

        public GrpcChannelProvider(ILogger<GrpcChannelProvider> logger)
        {
            _logger = logger;
        }

        public Task<GrpcChannel> GetChannel(string host, int port, bool noCertificateValidation = false, GrpcChannelOptions channelOptions = null)
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                var httpClient = new HttpClient(httpClientHandler);

                if (noCertificateValidation)
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback =
                        HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
                }

                if (channelOptions == null)
                {
                    channelOptions = new GrpcChannelOptions { HttpClient = httpClient };
                }
                else
                {
                    if (channelOptions.HttpClient == null)
                    {
                        channelOptions.HttpClient = httpClient;
                    }
                }
                channelOptions.DisposeHttpClient = true;

                return Task.FromResult(_channelMap.GetOrAdd($"https://{host}:{port}", x => GrpcChannel.ForAddress(x, channelOptions)));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Failed getting Channel for Host '{host}:{port}'");
                throw;
            }

        }
    }
}
