namespace SiLA2.Utils.Network
{
    using System;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Linq;
    using System.Collections.Generic;
    using System.Net.Sockets;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Networking utility library listing network interfaces and finding 
    /// interfaces based on name or CIDR
    /// </summary>
    public class NetworkService : INetworkService
    {
        private readonly ILogger<NetworkService> _logger;

        public NetworkService(ILogger<NetworkService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Finds the network interface that has a matching interface name or matching unicast address
        /// </summary>
        /// <param name="interfaceNameOrCIDR">interface name to find or the IPv4/IPv6 address or CIDR mask. </param>
        /// <returns>the matching found network interface</returns>
        public NetworkInterface GetNetworkInterface(string interfaceNameOrCIDR)
        {
            try
            {
                return GetNetworkInterfaceByCIDR(interfaceNameOrCIDR);
            }
            catch (NetworkInterfaceNotFoundException e)
            {
                _logger.LogWarning($"Could not get network interface by IP address or CIDR: {e.Message}");
            }
            catch (FormatException)
            {
                _logger.LogWarning($"'{interfaceNameOrCIDR}' is not an IP address or CIDR mask");
            }

            return GetNetworkInterfaceByName(interfaceNameOrCIDR);
        }

        public NetworkInterface GetNetworkInterfaceByName(string interfaceName)
        {
            if (interfaceName == null)
            {
                throw new NetworkInterfaceNotFoundException("No network interface name specified");
            }

            var nics = NetworkInterface.GetAllNetworkInterfaces();
            var nicByName = nics.SingleOrDefault(nic => nic.Name.ToLowerInvariant().Equals(interfaceName.ToLowerInvariant()));

            if(nicByName != null)
            {
                if(nicByName.OperationalStatus != OperationalStatus.Up)
                {
                    _logger.LogWarning($"Network interface '{nicByName.Name}' is not Up!");
                }
                else
                {
                    _logger.LogInformation($"Network interface '{nicByName.Name}' found!");
                    return nicByName;
                }

                var firstActiveNic = nics.FirstOrDefault(nic => nic.OperationalStatus == OperationalStatus.Up);
                if(firstActiveNic != null)
                {
                    _logger.LogInformation($"Using active network interface '{firstActiveNic.Name}' instead");
                    return firstActiveNic;
                }
            }
            
            throw new NetworkInterfaceNotFoundException($"Network interface '{interfaceName}' not found!");
        }

        /// <summary>
        /// Finds the first network interface that has a matching unicast address-
        /// </summary>
        /// <param name="mask">IPv4/IPv6 address or CIDR mask.</param>
        /// <returns>a matching interface</returns>
        public NetworkInterface GetNetworkInterfaceByCIDR(string mask)
        {
            if (mask.Contains('/'))
            {
                IPNetwork ipnetwork = IPNetwork.Parse(mask);
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                    if (!unicastAddresses.Any(ua => ipnetwork.Contains(ua.Address))) continue;
                    _logger.LogInformation($"Network interface '{nic.Name}' found!");
                    return nic;
                }
            }
            else
            {
                IPAddress address = IPAddress.Parse(mask);
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                    if (unicastAddresses.Select(ua => ua.Address).Contains(address))
                    {
                        _logger.LogInformation($"Network interface '{nic.Name}' found!");
                        return nic;
                    }
                }
            }
            throw new NetworkInterfaceNotFoundException($"No network interface found for IP address/CIDR Mask '{mask}'");
        }

        /// <summary>
        /// Finds a valid network interface on the system which is NOT loopback and is up
        /// </summary>
        /// <returns>a valid network interface</returns>
        public NetworkInterface FindValidNetworkInterface()
        {
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus != OperationalStatus.Up ||
                    nic.NetworkInterfaceType == NetworkInterfaceType.Loopback) continue;
                _logger.LogInformation($"Network interface '{nic.Name}' found!");
                return nic;
            }
            throw new NetworkInterfaceNotFoundException("No network interface was found that was UP and not 'Loopback'");
        }

        /// <summary>
        /// Creates a filtering func that can be passed to the Makaretu MulticastService.
        /// </summary>
        /// <param name="selectedInterfaces">Whitelist of network interfaces</param>
        /// <returns>
        /// A func that takes a list of interfaces and returns a list of those that are whitelisted.
        /// If no interfaces are passed to <para>selectedInterfaces</para>, no filter is applied.
        /// </returns>
        public Func<IEnumerable<NetworkInterface>, IEnumerable<NetworkInterface>> CreateFilterFunc(params NetworkInterface[] selectedInterfaces)
        {
            IEnumerable<NetworkInterface> FilterFunc(IEnumerable<NetworkInterface> nics)
            {
                if (selectedInterfaces.Length > 0)
                {
                    var filtered = nics.Where(ni => selectedInterfaces.Select(n => n.Id).Contains(ni.Id));
                    _logger.LogDebug($"Filtered {nics.Count()} interfaces down to {filtered.Count()} using a whitelist of {selectedInterfaces.Length}");
                    return filtered;
                }
                return nics;
            }
            return FilterFunc;
        }

        /// <summary>
        /// Enumerates the InterNetwork Unicast addresses for a given network interface.
        /// </summary>
        /// <param name="networkInterface">A network interface to inspect</param>
        /// <returns>list of InterNetwork Unicast IP v4 addresses</returns>
        public IEnumerable<IPAddress> ListInterNetworkAddresses(NetworkInterface networkInterface)
        {
            var unicastAddresses = networkInterface.GetIPProperties().UnicastAddresses.Select(u => u.Address);
            return unicastAddresses.Where(a => a.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}