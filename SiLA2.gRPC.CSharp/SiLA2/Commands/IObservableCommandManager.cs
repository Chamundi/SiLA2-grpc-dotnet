﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Commands
{
    public interface IObservableCommandManager<TParameter, TResponse>
    {
        Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func);
        Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func);
        void Dispose();
        ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid);
        Task RegisterForInfo(CommandExecutionUUID request, IServerStreamWriter<ExecutionInfo> responseStream, CancellationToken cancellationToken);
    }
}