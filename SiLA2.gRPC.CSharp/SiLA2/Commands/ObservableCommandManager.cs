using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;

namespace SiLA2.Commands
{
    /**
     * Class responsible for managing the observable commands utilizing the
     * ObservableCommandWrapper
     */
    public class ObservableCommandManager<TParameter, TResponse> : IDisposable, IObservableCommandManager<TParameter, TResponse>
    {
        private readonly ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>> _commands =
            new ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>>();

        private readonly ILogger<ObservableCommandManager<TParameter, TResponse>> _logger;
        private readonly ILoggerFactory _loggerFactory;

        public TimeSpan LifetimeOfExecution { get; } = TimeSpan.FromSeconds(60);

        /// <summary>
        /// Constructor of Observable command manager
        /// </summary>
        /// <param name="lifetimeOfExecution">The the duration during which a Command Execution UUID is valid.</param>
        public ObservableCommandManager(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _logger = _loggerFactory.CreateLogger<ObservableCommandManager<TParameter, TResponse>>();

            LifetimeOfExecution = TimeSpan.FromSeconds(configuration.GetValue<int>("CommandLifetimeInSeconds"));

            if (LifetimeOfExecution <= TimeSpan.Zero)
            {
                throw new InvalidOperationException(
                    $"Lifetime of execution must be greater than 0 but {LifetimeOfExecution} was specified");
            }
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        ///
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            TParameter parameter,
            Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory));
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        ///
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            TParameter parameter,
            Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken,TResponse> func)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory));
        }

        private Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            ObservableCommandWrapper<TParameter, TResponse> command)
        {
            if(_commands.TryAdd(command.Id, command))
            {
                _logger.LogDebug($"Added command: {command.Id}");
                // TODO: this execution should be handled by the task scheduler
                var response = command.Execute()
                .ContinueWith(task => Task.FromResult(command)).Unwrap();
                command.CommandExpired += onCommandExpired;
                return response;
            }
            else
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.CommandExecutionNotAccepted,
                    $"Command with Id {command.Id} could not be added!"));
                throw new InvalidOperationException();
            }
        }

        private void onCommandExpired(object sender, CommandExpiredArgs args)
        {
            RemoveCommand(args.CommandId, true);
        }

        private void RemoveCommand(Guid commandId, bool isExpired)
        {
            _logger.LogInformation($"Removing {(isExpired ? "expired " : string.Empty)}command: {commandId}");

            if (_commands.TryRemove(commandId, out var command))
            {
                command.Dispose();
            }
            else
            {
                _logger.LogWarning($"Could not remove command with ID {commandId}");
            }
        }

        /// <summary>
        /// Gets the observable command wrapper of a given command execution UUID from an Info stream
        /// </summary>
        /// <param name="commandExecutionUuid"></param>
        /// <returns></returns>
        public ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid)
        {
            var commandId = Guid.Parse(commandExecutionUuid.Value);
            return GetCommand(commandId);
        }

        private ObservableCommandWrapper<TParameter, TResponse> GetCommand(Guid commandId)
        {
            if (!_commands.TryGetValue(commandId, out var command))
            {
                var message = $"No such command with ID {commandId} was found";
                _logger.LogWarning(message);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid,
                    message));
            }

            return command;
        }

        /// <summary>
        /// Cleans up all observable command wrappers held by the manager. Tasks will be cancelled.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Registers response stream for command execution info updates for a given command UUID
        /// </summary>
        /// <param name="request">Request containing command ID to register</param>
        /// <param name="responseStream">client response stream to register command execution info updates to</param>
        /// <param name="cancellationToken">cancellation token from open client connection</param>
        /// <returns></returns>
        public async Task RegisterForInfo(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream,
            CancellationToken cancellationToken)
        {
            var command = GetCommand(request);
            await command.AddInfoObserver(responseStream, cancellationToken);
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    GetCommand(request);
                    await Task.Delay(Constants.AWAIT_PERIOD, cancellationToken);
                }
            }
            catch (TaskCanceledException)
            {
                _logger.LogDebug($"Cancelled command: {request.Value}");
            }
        }
    }
}
