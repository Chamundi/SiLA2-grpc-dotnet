namespace SiLA2.Server
{
    using System;
    using Grpc.Net.Client;

    public class ServerData
    {
        public GrpcChannel Channel { get; }
        public ServerConfig Config { get; }
        public ServerInformation Info { get; }

        /// <summary>
        /// Server model constructor (immutable object)
        /// </summary>
        /// <param name="channel">gRPC channel to connect to server</param>
        /// <param name="serverConfig">Server configuration data</param>
        /// <param name="serverInformation">Server information</param>
        public ServerData(GrpcChannel channel, ServerConfig serverConfig, ServerInformation serverInformation)
        {
            Config = serverConfig;
            Info = serverInformation;
            Channel = channel;
        }

        /// <summary>
        /// Overriden method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Config}{Environment.NewLine}{Info}{Environment.NewLine}gRPC Channel: {Channel.Target}";
        }
    }
}