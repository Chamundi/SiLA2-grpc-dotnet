﻿using Grpc.Net.Client;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using System;
using System.Threading.Tasks;
using SiLA2.Network;

namespace SiLA2.Server
{
    public class ServerDataProvider : IServerDataProvider
    {
        private readonly IGrpcServerChannelProvider _grpcServerChannelProvider;

        public ServerDataProvider(IGrpcServerChannelProvider grpcServerChannelProvider)
        {
            _grpcServerChannelProvider = grpcServerChannelProvider;
        }

        public async Task<ServerData> GetServerData()
        {
            return GetServerData(await _grpcServerChannelProvider.GetLocalServerChannel());
        }

        public ServerData GetServerData(GrpcChannel channel)
        {
            var silaService = new SiLAService.SiLAServiceClient(channel);
            var config = GetServerConfig(silaService);
            var info = GetServerInfo(silaService);
            var server = new ServerData(channel, config, info);
            return server;
        }

        private ServerConfig GetServerConfig(SiLAService.SiLAServiceClient silaService)
        {
            var nameResponse = silaService.Get_ServerName(new Get_ServerName_Parameters());
            var uuidRsponsse = silaService.Get_ServerUUID(new Get_ServerUUID_Parameters());
            return new ServerConfig(nameResponse.ServerName.Value, Guid.Parse(uuidRsponsse.ServerUUID.Value));
        }

        private ServerInformation GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var typeResponse = silaService.Get_ServerType(new Get_ServerType_Parameters());
            var descriptionResponse = silaService.Get_ServerDescription(new Get_ServerDescription_Parameters());
            var vendorUriResponse = silaService.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters());
            var versionResponse = silaService.Get_ServerVersion(new Get_ServerVersion_Parameters());
            return new ServerInformation(typeResponse.ServerType.Value, descriptionResponse.ServerDescription.Value, vendorUriResponse.ServerVendorURL.Value, versionResponse.ServerVersion.Value);
        }
    }
}
