﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public interface IBinaryDataRepository
    {
        Dictionary<Guid, byte[][]> UploadedData { get; }

        Dictionary<Guid, byte[]> DownloadableData { get; }

        Task<byte[]> GetUploadedData(Guid binaryTransferUUID);

        Task<Guid> CheckBinaryTransferUUID(string binaryTransferUUIDString, bool upload);
    }
}