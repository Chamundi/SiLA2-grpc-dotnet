﻿using Google.Protobuf;
using Grpc.Core;
using Sila2.Org.Silastandard;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public class BinaryDataRepository : IBinaryDataRepository
    {
        //TODO: Use ConcurrentDictionaries instead of regular Dictionaries ?
        public Dictionary<Guid, byte[][]> UploadedData { get; } = new Dictionary<Guid, byte[][]>();

        public Dictionary<Guid, byte[]> DownloadableData { get; } = new Dictionary<Guid, byte[]>();

        public async Task<Guid> CheckBinaryTransferUUID(string binaryTransferUUIDString, bool upload)
        {
            return await Task.Run(() =>
            {
                if (!Guid.TryParse(binaryTransferUUIDString, out var binaryTransferUUID) ||
                upload && !UploadedData.ContainsKey(binaryTransferUUID) ||
                !upload && !DownloadableData.ContainsKey(binaryTransferUUID))
                {
                    var error = new BinaryTransferError
                    {
                        ErrorType = BinaryTransferError.Types.ErrorType.InvalidBinaryTransferUuid,
                        Message = $"The given Binary Transfer UUID '{binaryTransferUUIDString}' is not valid"
                    };

                    throw new RpcException(new Status(StatusCode.Aborted, Convert.ToBase64String(error.ToByteArray())));
                }

                return binaryTransferUUID;
            });
        }

        public async Task<byte[]> GetUploadedData(Guid binaryTransferUUID)
        {
            return await Task.Run(() =>
            {
                List<byte> result = new List<byte>();

                if (UploadedData.ContainsKey(binaryTransferUUID))
                {
                    for (int i = 0; i < UploadedData[binaryTransferUUID].Length; i++)
                    {
                        var chunk = UploadedData[binaryTransferUUID][i];
                        if (chunk == null)
                        {
                            var error = new BinaryTransferError
                            {
                                ErrorType = BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                                Message = $"Not all chunks have been uploaded for Binary Transfer UUID '{binaryTransferUUID}' - no chunk with index {i} found"
                            };

                            throw new RpcException(new Status(StatusCode.Aborted, Convert.ToBase64String(error.ToByteArray())));
                        }

                        result.AddRange(chunk);
                    }
                }

                return result.ToArray();
            });
        }
    }
}