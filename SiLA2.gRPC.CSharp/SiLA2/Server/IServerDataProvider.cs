﻿using Grpc.Net.Client;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public interface IServerDataProvider
    {
        ServerData  GetServerData(GrpcChannel channel);
        Task<ServerData> GetServerData();
    }
}