﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using System;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public class SiLABinaryUploadService : BinaryUpload.BinaryUploadBase
    {
        private readonly ISiLA2Server _siLA2Server;
        private readonly IBinaryDataRepository _binaryDataRepository;
        private readonly ILogger<SiLABinaryUploadService> _logger;

        public SiLABinaryUploadService(ISiLA2Server siLA2Server, IBinaryDataRepository binaryDataRepository, ILogger<SiLABinaryUploadService> logger)
        {
            _siLA2Server = siLA2Server;
            _binaryDataRepository = binaryDataRepository;
            _logger = logger;
        }

        #region Overrides of BinaryUploadBase

        public override Task<CreateBinaryResponse> CreateBinary(CreateBinaryRequest request, ServerCallContext context)
        {
            try
            {
                // check if specified parameter is defined in a feature and has Binary type
                var feature = _siLA2Server.GetFeatureOfElement(request.ParameterIdentifier);
                if (feature == null)
                {
                    // specified feature does not exist
                    Utils.ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"Found no implemented feature that matches the parameter identifier '{request.ParameterIdentifier}'");
                }

                var obj = feature.GetMatchingElement(request.ParameterIdentifier);
                if (obj == null)
                {
                    // specified command or parameter does not exist
                    Utils.ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                        $"No command/parameter found that matches the parameter identifier '{request.ParameterIdentifier}'");
                }

                //TODO: compare size with available resources

                // create storage for data
                Guid binaryTransferUUID = Guid.NewGuid();
                _binaryDataRepository.UploadedData[binaryTransferUUID] = new byte[request.ChunkCount][];

                return Task.FromResult(new CreateBinaryResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    LifetimeOfBinary = new Duration()   ////TODO: add lifetime of binary data
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return null;
            }
        }

        public override async Task UploadChunk(IAsyncStreamReader<UploadChunkRequest> requestStream, IServerStreamWriter<UploadChunkResponse> responseStream, ServerCallContext context)
        {
            try
            {
                while (await requestStream.MoveNext(context.CancellationToken))
                {
                    var binaryTransferUUID = await _binaryDataRepository.CheckBinaryTransferUUID(requestStream.Current.BinaryTransferUUID, true);

                    // set received data chunk
                    _binaryDataRepository.UploadedData[binaryTransferUUID][requestStream.Current.ChunkIndex] = requestStream.Current.Payload.ToByteArray();

                    await responseStream.WriteAsync(new UploadChunkResponse
                    {
                        BinaryTransferUUID = binaryTransferUUID.ToString(),
                        ChunkIndex = requestStream.Current.ChunkIndex,
                        LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
        }

        public override async Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            try
            {
                var binaryTransferUUID = await _binaryDataRepository.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

                _binaryDataRepository.UploadedData.Remove(binaryTransferUUID);   ////TODO: check whether this crashes with a parallel running binary download

                return new DeleteBinaryResponse();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return null;
            }
        }

        #endregion
    }
}
