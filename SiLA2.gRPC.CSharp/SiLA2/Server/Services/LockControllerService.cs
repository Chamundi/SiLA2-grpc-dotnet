﻿namespace SiLA2.Server.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Timers;
    using Grpc.Core;
    using Microsoft.Extensions.Logging;
    using Sila2.Org.Silastandard;
    using Sila2.Org.Silastandard.Core.Lockcontroller.V1;
    using SiLA2.Server.Utils;
    using SiLA2.Utils;
    using Boolean = Sila2.Org.Silastandard.Boolean;
    using String = Sila2.Org.Silastandard.String;
    using GrpcCoreMetaData = Grpc.Core.Metadata;

    public class LockControllerService : LockController.LockControllerBase
    {
        #region Private members

        private const string FullyQualifiedFeatureIdentifier = "org.silastandard/core/LockController/v1";

        private string _currentLockIdentifier = string.Empty;
        private TimeSpan _lockTimeout;
        private readonly Timer _lockTimeoutTimer;
        private readonly ILogger<LockControllerService> _logger;
        private bool IsServerLocked => !string.IsNullOrEmpty(_currentLockIdentifier);

        #endregion

        #region Properties

        private Feature SiLAFeature { get; }

        /// <summary>
        /// Contains a list of fully qualified identifiers of Features,
        /// Commands and Properties that are affected by locking the Server.
        /// </summary>
        public List<string> LockableItems { private get; set; }

        #endregion

        #region Constructors and destructors

        public LockControllerService(ISiLA2Server silaServer, ILogger<LockControllerService> logger)
        {
            LockableItems = new List<string>();

            SiLAFeature = silaServer.GetFeature(FullyQualifiedFeatureIdentifier);

            // create lockout timeout timer
            _lockTimeoutTimer = new Timer
            {
                AutoReset = false,
                Enabled = false
            };

            _lockTimeoutTimer.Elapsed += OnLockTimeoutExpired;
            _logger = logger;
        }

        #endregion

        #region Overrides of LockControllerBase

        public override Task<Get_FCPAffectedByMetadata_LockIdentifier_Responses> Get_FCPAffectedByMetadata_LockIdentifier(Get_FCPAffectedByMetadata_LockIdentifier_Parameters request, ServerCallContext context)
        {
            Get_FCPAffectedByMetadata_LockIdentifier_Responses response = new();
            foreach (var item in LockableItems)
            {
                response.AffectedCalls.Add(new String { Value = item });
            }

            return Task.FromResult(response);
        }

        public override Task<Get_IsLocked_Responses> Get_IsLocked(Get_IsLocked_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_IsLocked_Responses { IsLocked = new Boolean { Value = IsServerLocked } });
        }

        public override Task<LockServer_Responses> LockServer(LockServer_Parameters request, ServerCallContext context)
        {
            // check if server is already locked
            if (IsServerLocked)
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "ServerAlreadyLocked");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            _currentLockIdentifier = request.LockIdentifier.Value;

            // if timeout has been set: setup and start lock timeout timer
            _lockTimeout = new TimeSpan(0, 0, (int)request.Timeout.Value);
            if (_lockTimeout.TotalSeconds > 0)
            {
                _lockTimeoutTimer.Interval = _lockTimeout.TotalMilliseconds;
                _lockTimeoutTimer.Enabled = true;
            }

            _logger.LogInformation($"Server locked.{(_lockTimeoutTimer.Enabled ? "Lock timeout timer started." : string.Empty)}");

            return Task.FromResult(new LockServer_Responses());
        }

        public override Task<UnlockServer_Responses> UnlockServer(UnlockServer_Parameters request, ServerCallContext context)
        {
            // check if server is locked
            if (!IsServerLocked)
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "ServerNotLocked");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            // check if lock identifier is valid
            if (!_currentLockIdentifier.Equals(request.LockIdentifier.Value))
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "InvalidLockIdentifier");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            _lockTimeoutTimer.Enabled = false;
            _currentLockIdentifier = string.Empty;

            _logger.LogInformation("Server unlocked.");

            return Task.FromResult(new UnlockServer_Responses());
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Checks whether a lock identifier is required.
        /// In that case the given metadata is checked for valid lock identifier. An error is raised if no valid lock identifier is found.
        /// </summary>
        /// <param name="metadata">The metadata contained in the request headers of the received client request.</param>
        public void CheckLock(GrpcCoreMetaData metadata)
        {
            if (!IsServerLocked)
            {
                // no lock identifier required if server is not locked
                return;
            }

            // get value of lock identifier metadata entry
            byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(metadata, SiLAFeature.GetFullyQualifiedMetadataIdentifier("LockIdentifier"));

            try
            {
                // extract lock identifier object
                var lockIdentifier = Metadata_LockIdentifier.Parser.ParseFrom(value);

                if (lockIdentifier?.LockIdentifier == null)
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "Lock identifier metadata value could not be parsed (wrong message type)"));
                    return;
                }

                // compare lock identifier
                if (!_currentLockIdentifier.Equals(lockIdentifier.LockIdentifier.Value))
                {
                    // raise defined execution error
                    var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "InvalidLockIdentifier");
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
                }
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, ErrorHandling.HandleException(e)));
            }

            // if lock timeout timer is running: reset timer
            if (_lockTimeoutTimer.Enabled)
            {
                _lockTimeoutTimer.Interval = _lockTimeout.TotalMilliseconds;
                _logger.LogDebug("Lock timeout timer reset.");
            }
        }

        #endregion

        #region Internal Methods

        private void OnLockTimeoutExpired(object source, ElapsedEventArgs e)
        {
            // unlock the server
            _lockTimeoutTimer.Enabled = false;
            _currentLockIdentifier = string.Empty;

            _logger.LogInformation($"Lock timeout timer elapsed after {_lockTimeoutTimer.Interval } ms. Server has been automatically unlocked.");
        }

        #endregion
    }
}