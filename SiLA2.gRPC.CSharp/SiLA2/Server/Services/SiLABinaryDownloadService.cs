﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public class SiLABinaryDownloadService : BinaryDownload.BinaryDownloadBase
    {
        private readonly IBinaryDataRepository _binaryDataRepository;
        private readonly ILogger<SiLABinaryDownloadService> _logger;

        public SiLABinaryDownloadService(IBinaryDataRepository binaryDataRepository, ILogger<SiLABinaryDownloadService> logger)
        {
            _binaryDataRepository = binaryDataRepository;
            _logger = logger;
        }

        #region Overrides of BinaryDownloadBase

        public override async Task<GetBinaryInfoResponse> GetBinaryInfo(GetBinaryInfoRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = await _binaryDataRepository.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

            return new GetBinaryInfoResponse
            {
                BinarySize = (ulong)_binaryDataRepository.DownloadableData[binaryTransferUUID].Length,
                LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
            };
        }

        public override async Task GetChunk(IAsyncStreamReader<GetChunkRequest> requestStream, IServerStreamWriter<GetChunkResponse> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext(context.CancellationToken))
            {
                var binaryTransferUUID = await _binaryDataRepository.CheckBinaryTransferUUID(requestStream.Current.BinaryTransferUUID, false);

                // get data chunk
                var chunk = ByteString.CopyFrom(_binaryDataRepository.DownloadableData[binaryTransferUUID], (int)requestStream.Current.Offset, (int)requestStream.Current.Length);

                await responseStream.WriteAsync(new GetChunkResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    Offset = requestStream.Current.Offset,
                    Payload = chunk,
                    LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                });
            }
        }

        public override async Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = await _binaryDataRepository.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

            _binaryDataRepository.DownloadableData.Remove(binaryTransferUUID);   ////TODO. check whether this crashes with a parallel running binary download

            return new DeleteBinaryResponse();
        }

        #endregion
    }
}
