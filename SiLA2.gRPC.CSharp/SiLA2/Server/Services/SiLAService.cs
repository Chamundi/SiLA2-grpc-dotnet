﻿using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.Server.Utils;
using SiLA2.Utils;
using SiLA2.Utils.Config;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.Server.Services
{

    public class SiLAService : SiLAServiceBase
    {
        public ServerInformation ServerInfo { get; set; }

        private readonly ISiLA2Server _siLA2Server;
        private readonly IConfiguration _configuration;
        private readonly IWritableOptions<ServerConfig> _writableServerConfigSection;
        private readonly ILogger<SiLAService> _logger;

        public Feature SiLAFeature { get; set; }

        public SiLAService(ISiLA2Server siLA2Server, IConfiguration configuration, IWritableOptions<ServerConfig> writableServerConfigSection, ILogger<SiLAService> logger)
        {
            _siLA2Server = siLA2Server;
            _configuration = configuration;
            _writableServerConfigSection = writableServerConfigSection;
            _logger = logger;
        }

        public override Task<GetFeatureDefinition_Responses> GetFeatureDefinition(GetFeatureDefinition_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request -> ");

            // lookup feature by given qualified identifier
            if (!_siLA2Server.ImplementedFeatures.Contains(request.FeatureIdentifier.Value))
            {
                // get error identifier and message from defined execution error specified in the feature definition (SiLAService Feature)
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "UnimplementedFeature");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(
                    SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier),
                    $"Feature definition '{request.FeatureIdentifier.Value}' is probably missing. Check that the requested feature definition is valid and available"));
            }

            // get feature from server
            var feature = _siLA2Server.GetFeature(request.FeatureIdentifier.Value);

            // serialize Feature definition
            var serializer = new XmlSerializer(typeof(Feature));
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new ExtentedStringWriter(stringBuilder, new UTF8Encoding(false)))
            {
                serializer.Serialize(stringWriter, feature);
            }

            return Task.FromResult(new GetFeatureDefinition_Responses { FeatureDefinition = new SiLAFramework.String { Value = stringBuilder.ToString().Replace(Environment.NewLine, string.Empty) } });
        }

        //TODO: Write ServerName to appSettings.json and update it in SiLAServer
        public override Task<SetServerName_Responses> SetServerName(SetServerName_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request ->  Response = \"{request.ServerName}\"");

            _writableServerConfigSection.Update(opt => opt.Name = request.ServerName.Value);

            return Task.FromResult(new SetServerName_Responses());
        }

        public override Task<Get_ServerName_Responses> Get_ServerName(Get_ServerName_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            var serverId = _configuration.GetSection("ServerConfig:Name").Value;
            try
            {
                _logger.LogInformation($"Request ->  Response = \"{serverId}\"");
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
            }
            return Task.FromResult(new Get_ServerName_Responses { ServerName = new SiLAFramework.String { Value = serverId } });
        }

        public override Task<Get_ServerUUID_Responses> Get_ServerUUID(Get_ServerUUID_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            var serverId = _configuration.GetSection("ServerConfig:UUID").Value;
            _logger.LogTrace($"Request ->  Response = \"{serverId}\"");

            return Task.FromResult(new Get_ServerUUID_Responses { ServerUUID = new SiLAFramework.String { Value = serverId } });
        }

        public override Task<Get_ServerType_Responses> Get_ServerType(Get_ServerType_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request ->  Response = \"{_siLA2Server.ServerInformation.Type}\"");

            return Task.FromResult(new Get_ServerType_Responses { ServerType = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Type } });
        }

        public override Task<Get_ServerDescription_Responses> Get_ServerDescription(Get_ServerDescription_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request ->  Response = \"{_siLA2Server.ServerInformation.Description}\"");

            return Task.FromResult(new Get_ServerDescription_Responses { ServerDescription = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Description } });
        }

        public override Task<Get_ServerVendorURL_Responses> Get_ServerVendorURL(Get_ServerVendorURL_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request ->  Response = \"{_siLA2Server.ServerInformation.VendorURI}\"");

            return Task.FromResult(new Get_ServerVendorURL_Responses { ServerVendorURL = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.VendorURI } });
        }

        public override Task<Get_ServerVersion_Responses> Get_ServerVersion(Get_ServerVersion_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation($"Request ->  Response = \"{_siLA2Server.ServerInformation.Version}\"");

            return Task.FromResult(new Get_ServerVersion_Responses { ServerVersion = new SiLAFramework.String { Value = _siLA2Server.ServerInformation.Version } });
        }

        public override Task<Get_ImplementedFeatures_Responses> Get_ImplementedFeatures(Get_ImplementedFeatures_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            _logger.LogInformation("Request -> ");

            var response = new Get_ImplementedFeatures_Responses();
            foreach (var f in _siLA2Server.ImplementedFeatures)
            {
                response.ImplementedFeatures.Add(new SiLAFramework.String { Value = f });
            }

            return Task.FromResult(response);
        }

        private void CheckMetadata(ServerCallContext context)
        {
            if (SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders).Count > 0)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.NoMetadataAllowed, "Requests of the SiLAService feature MUST NOT contain SiLA Client Metadata"));
            }
        }

        private sealed class ExtentedStringWriter : StringWriter
        {
            public ExtentedStringWriter(StringBuilder builder, Encoding desiredEncoding)
                : base(builder)
            {
                Encoding = desiredEncoding;
            }

            public override Encoding Encoding { get; }
        }
    }
}
