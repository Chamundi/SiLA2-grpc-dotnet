﻿using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;

namespace SiLA2.Server.Services
{
    public abstract class CloudEndPoint : CloudClientEndpoint.CloudClientEndpointBase
    {
        private readonly ILogger<CloudEndPoint> _logger;

        public CloudEndPoint(ILogger<CloudEndPoint> logger)
        {
            _logger = logger;
        }
    }
}
