﻿using Grpc.Core;
using System;
using System.Collections.Generic;

namespace SiLA2.Server
{
    public interface ISiLA2Server
    {
        ServerInformation ServerInformation { get; }
        List<string> ImplementedFeatures { get; }
        void Start();
        void AddFeature(string featureDefinitonFile, ServerServiceDefinition serviceDefinition);
        Feature GetFeature(string featureIdentifier);
        Feature ReadFeature(string featureDefinitionFile);
        Feature ReadFeature(string resourceName, Type implementationType);
        Feature GetFeatureOfElement(string fullyQualifiedIdentifier);
    }
}