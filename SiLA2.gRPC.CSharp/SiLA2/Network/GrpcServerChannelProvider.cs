﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SiLA2.Network
{
    public class GrpcServerChannelProvider : IGrpcServerChannelProvider
    {
        private readonly IConfiguration _config;
        private readonly ILogger<GrpcServerChannelProvider> _logger;
        private HttpClientHandler _httpClientHandler;
        private HttpClient _httpClient;

        public GrpcServerChannelProvider(IConfiguration config, ILogger<GrpcServerChannelProvider> logger)
        {
            _httpClientHandler = new HttpClientHandler() { };
            // TODO : Authentifizierung über Zertifikat
            _httpClientHandler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            _httpClient = new HttpClient(_httpClientHandler);
            _config = config;
            _logger = logger;
        }

        public Task<GrpcChannel> GetLocalServerChannel()
        {
            try
            {
                return Task.FromResult(GrpcChannel.ForAddress($"https://{_config.GetSection("ServerInfo:FQHN").Value}:{_config.GetSection("ServerInfo:Port").Value}", new GrpcChannelOptions { HttpClient = _httpClient, DisposeHttpClient = true }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return null;
            }
        }
    }
}
