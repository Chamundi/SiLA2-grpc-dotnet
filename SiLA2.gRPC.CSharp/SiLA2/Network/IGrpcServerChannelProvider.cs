﻿using Grpc.Net.Client;
using System.Threading.Tasks;

namespace SiLA2.Network
{
    public interface IGrpcServerChannelProvider
    {
        Task<GrpcChannel> GetLocalServerChannel();
    }
}
