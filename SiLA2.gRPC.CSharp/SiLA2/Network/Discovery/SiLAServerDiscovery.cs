using System;
using System.Collections.Generic;
using System.Linq;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLAFramework = Sila2.Org.Silastandard;

namespace SiLA2.Network.Discovery
{
    using Grpc.Core;
    using Microsoft.Extensions.Logging;
    using SiLA2.Network.Discovery.mDNS;
    using Grpc.Net.Client;
    using SiLA2.Server.Utils;
    using Microsoft.Extensions.Configuration;
    using System.Threading.Tasks;
    using SiLA2.Server;
    using SiLA2.Utils.gRPC;

    /// <summary>
    /// Exception to be used if Server is not found during discovery
    /// </summary>
    public class ServerNotFoundException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">reason why server was not found</param>
        /// <returns></returns>
        public ServerNotFoundException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Utility class to be used to discover and find SiLA Servers using the 
    /// ServiceFinder class.
    /// </summary>
    public class SiLAServerDiscovery : ISiLAServerDiscovery
    {
        private readonly IServiceFinder _serviceFinder;
        private readonly IGrpcChannelProvider _grpcChannelProvider;
        private readonly IServerDataProvider _serverDataProvider;
        private readonly IConfiguration _configuration;
        private readonly ILogger<SiLAServerDiscovery> _logger;

        public SiLAServerDiscovery(IServiceFinder serviceFinder, IGrpcChannelProvider grpcChannelProvider, IServerDataProvider serverDataProvider, IConfiguration configuration, ILogger<SiLAServerDiscovery> logger)
        {
            _serviceFinder = serviceFinder;
            _grpcChannelProvider = grpcChannelProvider;
            _serverDataProvider = serverDataProvider;
            _configuration = configuration;
            _logger = logger;
        }

        /// <summary>
        /// Query a list of SiLA2 servers with valid SiLAService
        /// </summary>
        /// <param name="timeout">duration of Query period</param>
        /// <param name="credentials">Ssl credentials created</param>
        /// <returns>"List of valid SiLA2 servers</returns>
        public async Task<IEnumerable<ServerData>> GetServers(int timeout = 10000, SslCredentials credentials = null)
        {

            string networkDevice = _configuration.GetSection("Connection:ServerDiscovery:NIC").Value;
            var nic = networkDevice.Equals("Default") ? Environment.OSVersion.Platform == PlatformID.Win32NT ? "Ethernet" : "eth0" : networkDevice;

            var foundServers = await _serviceFinder.GetConnections(_configuration.GetSection("Connection:ServerDiscovery:ServiceName").Value, nic, credentials, timeout);

            var serverDatas = new List<ServerData>();
            foreach (var server in foundServers)
            {
                try
                {
                    var channel = await _grpcChannelProvider.GetChannel(server.Address, server.Port, true);
                    var serverdata = _serverDataProvider.GetServerData(channel);
                    serverDatas.Add(serverdata);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"Invalid server found at channel: {server}{Environment.NewLine}{ex}");
                }
            }

            return serverDatas;
        }

        /// <summary>
        /// Load all server properties 
        /// </summary>
        public ServerData GetServerData(GrpcChannel channel)
        {
            var silaService = new SiLAService.SiLAServiceClient(channel);
            var config = GetServerConfig(silaService);
            var info = GetServerInfo(silaService);
            var server = new ServerData(channel, config, info);
            return server;
        }

        private Feature GetFeatureDefinition(SiLAService.SiLAServiceClient silaService, string featureIdentifier)
        {
            GetFeatureDefinition_Responses response;
            response = silaService.GetFeatureDefinition(new GetFeatureDefinition_Parameters
            {
                FeatureIdentifier = new SiLAFramework.String
                {
                    Value = featureIdentifier
                }
            });
            return FeatureGenerator.ReadFeatureFromXml(response.FeatureDefinition.Value);
        }

        private ServerConfig GetServerConfig(SiLAService.SiLAServiceClient silaService)
        {
            var name = silaService.Get_ServerName(new Get_ServerName_Parameters()).ServerName.Value;
            var uuid = Guid.Parse(silaService.Get_ServerUUID(new Get_ServerUUID_Parameters()).ServerUUID.Value);
            var config = new ServerConfig(name, uuid);
            return config;
        }

        private ServerInformation GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var type = silaService.Get_ServerType(new Get_ServerType_Parameters()).ServerType.Value;
            var description = silaService.Get_ServerDescription(new Get_ServerDescription_Parameters()).ServerDescription.Value;
            var vendorUri = silaService.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters()).ServerVendorURL.Value;
            var version = silaService.Get_ServerVersion(new Get_ServerVersion_Parameters()).ServerVersion.Value;
            var info = new ServerInformation(type, description, vendorUri, version);
            return info;
        }
    }
}