﻿using Microsoft.Extensions.Configuration;
using System;

namespace SiLA2.Network.Discovery
{
    public class ServiceDiscoveryInfo
    {
        public string ServerInstanceName { get; }
        public ushort Port { get; }
        public string NetworkInterface { get; }
        public string DiscoveryServiceName { get; }

        public ServiceDiscoveryInfo(IConfiguration configuration)
        {
            ServerInstanceName = configuration.GetSection("ServerConfig:UUID").Value;
            Port = Convert.ToUInt16(configuration.GetSection("ServerInfo:Port").Value);
            var networkDevice = configuration.GetSection("ServerInfo:NetworkInterface").Value;
            NetworkInterface = networkDevice.Equals("Default") ? Environment.OSVersion.Platform == PlatformID.Win32NT ? "Ethernet" : "eth0" : networkDevice;
            DiscoveryServiceName = configuration.GetSection("ServerInfo:DiscoveryServiceName").Value;
        }
    }
}


