﻿using Grpc.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Network.Discovery.mDNS
{
    public interface IServiceFinder
    {
        Task<IEnumerable<ConnectionInfo>> GetConnections(string serviceName, string networkInterface, SslCredentials credentials = null, int searchDuration = 10000);
    }
}