﻿using System;

namespace SiLA2.Network.Discovery.mDNS
{
    public interface IServiceAnnouncer : IDisposable
    {
        void Start();
    }
}