﻿using Grpc.Core;
using Makaretu.Dns;
using Microsoft.Extensions.Logging;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace SiLA2.Network.Discovery.mDNS
{
    public class ServiceFinder : IServiceFinder
    {
        private readonly INetworkService _networking;
        private readonly ILogger<ServiceFinder> _logger;

        public ServiceFinder(INetworkService networking, ILogger<ServiceFinder> logger)
        {
            _networking = networking;
            _logger = logger;
        }

        public async Task<IEnumerable<ConnectionInfo>> GetConnections(string serviceName, string networkInterface, SslCredentials credentials = null, int searchDuration = 10000)
        {
            var connections = new List<ConnectionInfo>();

            var networkInterfaces = new NetworkInterface[] { _networking.GetNetworkInterface(networkInterface) };
            using (var mdns = new MulticastService())
            {
                // attach the event handler for processing responses.
                mdns.AnswerReceived += (s, e) =>
                {
                    foreach (var item in e.Message.Answers)
                    {
                        Console.WriteLine($"{item}");
                        _logger.LogDebug($"{item}");
                    }
                    
                    // The first Answer will be a PTRRecord
                    var pointers = e.Message.Answers.OfType<PTRRecord>().Where(p => p.DomainName.ToString().Contains(serviceName));
                    foreach (var pointer in pointers)
                    {
                        // Ask for the service instance details
                        mdns.SendQuery(pointer.DomainName, type: DnsType.SRV);
                    }

                    // The second Answer will be a SRVRecord (also contain A and AAAA records)
                    var servers = e.Message.Answers.OfType<SRVRecord>().Where(p => p.ToString().Contains(serviceName));
                    foreach (var server in servers)
                    {
                        var A_record = e.Message.AdditionalRecords.OfType<ARecord>().FirstOrDefault();
                        if (A_record != null)
                        {
                            Console.WriteLine($"Discovered Service at {A_record.Address}:{server.Port}");
                            _logger.LogTrace($"Discovered Service at {A_record.Address}:{server.Port}");
                            connections.Add(new ConnectionInfo(A_record.Address.ToString(), server.Port));
                        }

                        var AAAA_record = e.Message.AdditionalRecords.OfType<AAAARecord>().FirstOrDefault();
                        if (AAAA_record != null)
                        {
                            Console.WriteLine($"Discovered Service at {AAAA_record.Address}:{server.Port}");
                            _logger.LogTrace($"Discovered Service at {AAAA_record.Address}:{server.Port}");
                            connections.Add(new ConnectionInfo(AAAA_record.Address.ToString(), server.Port));
                        }
                    }
                };

                mdns.Start();

                // Ask for PTRRecords (the devices will only respond to SRV that are directed exactly to them)
                var sn = string.Join(".", serviceName, "local");
                mdns.SendQuery(sn, type: DnsType.PTR);
                await Task.Delay(searchDuration);
                mdns.Stop();

                return connections;
            }
        }
    }
}
