﻿using Makaretu.Dns;
using Microsoft.Extensions.Logging;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SiLA2.Network.Discovery.mDNS
{
    public class ServiceAnnouncer : IServiceAnnouncer
    {
        private readonly ILogger<ServiceAnnouncer> _logger;

        private MulticastService _multicastService;
        private ServiceDiscovery _serviceDiscoverer;

        /// <summary>
        /// The service profile that is announced.
        /// </summary>
        public ServiceProfile Profile { get; private set; }

        /// <summary>
        /// The IP addresses and ports at which the service is being advertised.
        /// </summary>
        public IEnumerable<Tuple<IPAddress, int>> Endpoints
        {
            get
            {
                var addresses = new List<IPAddress>();
                if (Profile != null)
                {
                    int port = Profile.Resources.OfType<SRVRecord>().FirstOrDefault().Port;
                    addresses.AddRange(Profile.Resources.OfType<ARecord>().Select(r => r.Address));
                    addresses.AddRange(Profile.Resources.OfType<AAAARecord>().Select(r => r.Address));
                    return addresses.Select(a => new Tuple<IPAddress, int>(a, port));
                }
                return null;
            }
        }

        public ServiceAnnouncer(ServiceDiscoveryInfo connectionInfo, INetworkService networking, ILogger<ServiceAnnouncer> logger)
        {
            _logger = logger;

            var networkInterface = networking.GetNetworkInterface(connectionInfo.NetworkInterface);
            var selectedAddresses = networking.ListInterNetworkAddresses(networkInterface);
            var filterFunc = networking.CreateFilterFunc(networkInterface);

            Profile = new ServiceProfile(connectionInfo.ServerInstanceName, connectionInfo.DiscoveryServiceName, connectionInfo.Port, selectedAddresses);

            _multicastService = new MulticastService(filterFunc);
        }

        public void Start()
        {

            _serviceDiscoverer?.Dispose();
            _logger.LogInformation("Starting multicast service");
            _multicastService.Start();
            _serviceDiscoverer = new ServiceDiscovery(_multicastService);
            _logger.LogInformation($"Starting to advertise Service {Profile.FullyQualifiedName}");
            _serviceDiscoverer.Advertise(Profile);
        }


        public void Dispose()
        {
            _serviceDiscoverer?.Dispose();
            _serviceDiscoverer = null;
            _multicastService?.Dispose();
            _multicastService = null;
        }

        
    }
}
