﻿namespace SiLA2.Network.Discovery.mDNS
{
    public class ConnectionInfo
    {
        public string Address { get; }
        public int Port { get; }

        public ConnectionInfo(string address, int port)
        {
            Address = address;
            Port = port;
        }

        public override string ToString()
        {
            return $"https://{Address}:{Port}";
        }
    }
}
