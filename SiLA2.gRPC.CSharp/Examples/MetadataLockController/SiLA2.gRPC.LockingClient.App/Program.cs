﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Client;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;

namespace SiLA2.gRPC.LockingClient.App
{
    internal class Program
    {
        private static IConfigurationRoot _configuration;


        static async Task Main(string[] args)
        {
            try
            {
                var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                _configuration = configBuilder.Build();

                var clientSetup = new Configurator(_configuration);

                Console.WriteLine("Starting Server Discovery...");

                var serverMap = await clientSetup.SearchForServers();

                GrpcChannel channel;
                var serverType = "SiLA2 Metadata LockController Server";
                var server = serverMap.Values.FirstOrDefault(x => x.Info.Type == serverType);
                if (server != null)
                {
                    Console.WriteLine($"Connecting to {server}");
                    channel = server.Channel;
                }
                else
                {
                    var fqhn = _configuration["Connection:FQHN"];
                    var port = int.Parse(_configuration["Connection:Port"]);
                    Console.WriteLine($"No connection automatically discovered. Using Server-URI '{fqhn}:{port}' from appSettings.config");
                    channel = await clientSetup.ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(fqhn, port, true);
                }

                Console.WriteLine("Trying to setup Client ...");
                var clientImpl = new Client(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());
                Console.WriteLine($"Client Setup successful for following Clients :");
                foreach (var client in clientImpl.Clients)
                {
                    Console.WriteLine($"{client.GetType().FullName}");
                }

                // command Get_FCPAffectedByMetadata_LockIdentifier
                var identifiers = clientImpl.GetLockAffectedIdentifiers;
                Console.WriteLine("\nIdentifiers affected by locking:");
                foreach (var i in identifiers)
                {
                    Console.WriteLine($" - '{i}'");
                }

                // property CurrentYear
                Console.WriteLine("\n--> request: \"Get_StartYear()\" ...");
                Console.WriteLine($"<-- response: \"{clientImpl.GetStartYear()}\"");

                // command SayHello
                var param = "SiLA user";
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" ...");
                Console.WriteLine($"<-- response: \"{clientImpl.SayHello(param)}\"");

                // check log state
                Console.WriteLine("\n--> request: \"IsServerLocked()\" ...");
                var locked = clientImpl.IsServerLocked;
                if (locked != null)
                {
                    Console.WriteLine($"<-- response: \"{(bool)locked}\"");
                }

                // lock server
                var lockIdentifier = "MY_LOCK_ID";
                int timeout = 0;
                Console.WriteLine($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                // check log state
                Console.WriteLine("\n--> request: \"IsServerLocked()\" ...");
                locked = clientImpl.IsServerLocked;
                if (locked != null)
                {
                    Console.WriteLine($"<-- response: \"{(bool)locked}\"");
                }

                // try to lock server (already in lock state)
                Console.WriteLine($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                Thread.Sleep(1000);

                // try to call SiLAService property with metadata (not allowed)
                Console.WriteLine($"\n--> request: \"Get_ImplementedFeatures()\" (with metadata lock identifier  '{lockIdentifier}') ...");
                clientImpl.CallSiLAServicePropertyWithMetadata(lockIdentifier);

                Thread.Sleep(1000);

                // property CurrentYear
                Console.WriteLine("\n--> request: \"Get_StartYear()\" (not locked) ...");
                Console.WriteLine($"<-- response: \"{clientImpl.GetStartYear()}\"");

                // command SayHello (without required metadata)
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (without metadata) ...");
                clientImpl.SayHello(param);

                Thread.Sleep(1000);

                // command SayHello (with wrong metadata key)
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with wrong metadata key) ...");
                clientImpl.SayHelloWithWrongLockIdentifierKey(param, lockIdentifier);

                Thread.Sleep(1000);

                // command SayHello (with metadata containing no Metadata_LockIdentifier but a SiLA Integer type)
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with Integer SiLA type in metadata content) ...");
                clientImpl.SayHelloWithIntegerLockIdentifier(param, 42);

                Thread.Sleep(1000);

                // command SayHello with lock identifier format - not a gRPC message
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with wrong metadata content format) ...");
                clientImpl.SayHelloWithWrongFormatLockIdentifierContent(param, lockIdentifier);

                Thread.Sleep(1000);

                // command SayHello (with metadata containing invalid lock identifier)
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier 'something_else') ...");
                clientImpl.SayHelloWithLockIdentifier(param, "something_else");

                Thread.Sleep(1000);

                // command SayHello (with required metadata)
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier '{lockIdentifier}') ...");
                Console.WriteLine($"<-- response: \"{clientImpl.SayHelloWithLockIdentifier(param, lockIdentifier)}\"");

                // try unlock server without valid lock identifier
                Console.WriteLine("\n--> request: \"UnlockServer(LockIdentifier='just_guessing')\" ...");
                clientImpl.UnlockServer("just_guessing");

                Thread.Sleep(1000);

                // unlock server with valid lock identifier
                Console.WriteLine($"\n--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                clientImpl.UnlockServer(lockIdentifier);

                // check log state 
                Console.WriteLine("\n--> request: \"IsServerLocked()\" ...");
                locked = clientImpl.IsServerLocked;
                if (locked != null)
                {
                    Console.WriteLine($"<-- response: \"{(bool)locked}\"");
                }

                // try to unlock server (not locked)
                Console.WriteLine($"\n--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                clientImpl.UnlockServer(lockIdentifier);

                Thread.Sleep(1000);

                // lock server with a timeout
                lockIdentifier = "MY_NEW_LOCK_ID";
                timeout = 5;
                Console.WriteLine($"\n--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout}s)\" ...");
                clientImpl.LockServer(lockIdentifier, timeout);

                Console.WriteLine("Waiting for 4 seconds ...");
                Thread.Sleep(4000);

                // command SayHello (with required metadata) to reset the lock timeout timer
                Console.WriteLine($"\n--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier  '{lockIdentifier}') -> resets the lock timeout");
                Console.WriteLine($"<-- response: \"{clientImpl.SayHelloWithLockIdentifier(param, lockIdentifier)}\"");

                Console.WriteLine("Waiting for 4 seconds ...");

                // check log state 
                Console.WriteLine("\n--> request: \"IsServerLocked()\" ...");
                locked = clientImpl.IsServerLocked;
                if (locked != null)
                {
                    Console.WriteLine($"<-- response: \"{(bool)locked}\"");
                }

                Console.WriteLine("Waiting for 5 seconds to elapse the lock timeout...");
                Thread.Sleep(5000);

                // check log state 
                Console.WriteLine("\n--> request: \"IsServerLocked()\" ...");
                locked = clientImpl.IsServerLocked;
                if (locked != null)
                {
                    Console.WriteLine($"<-- response: \"{(bool)locked}\"");
                }

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error: {0}", e);
            }

            Console.ReadKey();
        }
    }
}
