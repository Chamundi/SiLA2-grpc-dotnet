﻿using ErrorRecovery.Features.Services;
using SiLA2.Commands;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Utils;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System.Reflection;

namespace SiLA2.gRPC.ErrorRecovery.Server.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(x => x.EnableDetailedErrors = true);
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<ErrorRecoveryServiceImpl>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddSingleton<IGrpcServerChannelProvider, GrpcServerChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<RecoverableErrorProviderImpl>();
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: RecoverableErrorProviderImpl _ is injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, RecoverableErrorProviderImpl _, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<RecoverableErrorProviderImpl>();
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<ErrorRecoveryServiceImpl>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
