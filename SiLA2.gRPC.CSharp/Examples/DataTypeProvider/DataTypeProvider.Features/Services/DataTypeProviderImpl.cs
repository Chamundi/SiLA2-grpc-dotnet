﻿using System.Text;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using SiLA2;
using SiLA2.Server;
using Sila2.Org.Silastandard.Examples.Datatypeprovider.V1;
using SiLA2.Server.Utils;
using SiLAFramework = Sila2.Org.Silastandard;

namespace DataTypeProvider.Features.Services
{
    /// <summary>
    /// Implements the functionality of the DataTypeProvider feature.
    /// </summary>
    public class DataTypeProviderImpl : SiLAFramework.Examples.Datatypeprovider.V1.DataTypeProvider.DataTypeProviderBase //, IDisposable
    {
        #region Private members

        private readonly Feature _silaFeature;

        private readonly IBinaryDataRepository _binaryDataRepository;

        private readonly ILogger<DataTypeProviderImpl> _logger;

        #endregion

        #region Constructors and destructors

        public DataTypeProviderImpl(ISiLA2Server silaServer, IBinaryDataRepository binaryData, ILogger<DataTypeProviderImpl> logger)
        {
            _binaryDataRepository = binaryData;
            _logger = logger;
            _silaFeature = silaServer.ReadFeature(Path.Combine("Features", "DataTypeProvider.sila.xml"));
        }

        #endregion

        #region Overrides of DataTypeProviderBase

        #region String

        public override Task<SetStringValue_Responses> SetStringValue(SetStringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetStringValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received String value: '{request.StringValue.Value}'" } });
        }

        public override Task<Get_StringValue_Responses> Get_StringValue(Get_StringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StringValue_Responses { StringValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" } });
        }

        #endregion

        #region Integer

        public override Task<SetIntegerValue_Responses> SetIntegerValue(SetIntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetIntegerValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Integer value: {request.IntegerValue.Value}" } });
        }

        public override Task<Get_IntegerValue_Responses> Get_IntegerValue(Get_IntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_IntegerValue_Responses { IntegerValue = new SiLAFramework.Integer { Value = 5124 } });
        }

        #endregion

        #region Real

        public override Task<SetRealValue_Responses> SetRealValue(SetRealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetRealValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Real value: {request.RealValue.Value}" } });
        }

        public override Task<Get_RealValue_Responses> Get_RealValue(Get_RealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_RealValue_Responses { RealValue = new SiLAFramework.Real { Value = 3.1415926 } });
        }

        #endregion

        #region Boolean

        public override Task<SetBooleanValue_Responses> SetBooleanValue(SetBooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new SetBooleanValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Boolean value: {request.BooleanValue.Value}" } });
        }

        public override Task<Get_BooleanValue_Responses> Get_BooleanValue(Get_BooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BooleanValue_Responses { BooleanValue = new SiLAFramework.Boolean { Value = true } });
        }

        #endregion

        #region Binary

        public override Task<SetBinaryValue_Responses> SetBinaryValue(SetBinaryValue_Parameters request, ServerCallContext context)
        {
            // check type of binary data
            switch (request.BinaryValue.UnionCase)
            {
                case SiLAFramework.Binary.UnionOneofCase.Value:
                    return Task.FromResult(new SetBinaryValue_Responses
                    {
                        ReceivedValue = new SiLAFramework.Binary { Value = request.BinaryValue.Value },
                        TransferMode = new SiLAFramework.String { Value = "Direct" }
                    });

                case SiLAFramework.Binary.UnionOneofCase.BinaryTransferUUID:

                    // check for available upload result
                    Guid receivedDataUUID = _binaryDataRepository.CheckBinaryTransferUUID(request.BinaryValue.BinaryTransferUUID, true).Result;

                    // copy received data and provide it as response
                        Guid responseDataUUID = Guid.NewGuid();
                        _binaryDataRepository.DownloadableData.Add(responseDataUUID, _binaryDataRepository.GetUploadedData(receivedDataUUID).Result);

                        // create response
                        return Task.FromResult(new SetBinaryValue_Responses
                        {
                            ReceivedValue = new SiLAFramework.Binary { BinaryTransferUUID = responseDataUUID.ToString() },
                            TransferMode = new SiLAFramework.String { Value = "Upload" }
                        });
                    
            }

            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                _silaFeature.GetFullyQualifiedCommandParameterIdentifier("SetBinaryValue", "binaryValue"),
                "The value must either be 'Value' or 'BinaryTransferUUID'"));
            return null;
        }

        public override Task<Get_BinaryValueDirectly_Responses> Get_BinaryValueDirectly(Get_BinaryValueDirectly_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_BinaryValueDirectly_Responses
            {
                BinaryValueDirectly = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Test_String_Value") }
            });
        }

        public override Task<Get_BinaryValueDownload_Responses> Get_BinaryValueDownload(Get_BinaryValueDownload_Parameters request, ServerCallContext context)
        {
            // create binary download
            var data = $"A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download_created_at_{DateTime.Now.ToShortDateString()}_{DateTime.Now.ToShortTimeString()}";
            Guid binaryTransferUUID = Guid.NewGuid();
            _binaryDataRepository.DownloadableData.Add(binaryTransferUUID, ByteString.CopyFromUtf8(data).ToByteArray());

            return Task.FromResult(new Get_BinaryValueDownload_Responses
            {
                BinaryValueDownload = new SiLAFramework.Binary { BinaryTransferUUID = binaryTransferUUID.ToString() }
            });
        }

        #endregion

        #region Date

        public override Task<SetDateValue_Responses> SetDateValue(SetDateValue_Parameters request, ServerCallContext context)
        {
            var dateValue = new DateTime((int)request.DateValue.Year, (int)request.DateValue.Month, (int)request.DateValue.Day);

            return Task.FromResult(new SetDateValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Date value: {dateValue.Date}" } });
        }

        public override Task<Get_DateValue_Responses> Get_DateValue(Get_DateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_DateValue_Responses { DateValue = new SiLAFramework.Date { Year = 2018, Month = 8, Day = 24, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #region Time

        public override Task<SetTimeValue_Responses> SetTimeValue(SetTimeValue_Parameters request, ServerCallContext context)
        {
            var timeValue = new TimeSpan((int)request.TimeValue.Hour, (int)request.TimeValue.Minute, (int)request.TimeValue.Second);

            return Task.FromResult(new SetTimeValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Time value: {timeValue}" } });
        }

        public override Task<Get_TimeValue_Responses> Get_TimeValue(Get_TimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimeValue_Responses { TimeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #region Timestamp

        public override Task<SetTimestampValue_Responses> SetTimestampValue(SetTimestampValue_Parameters request, ServerCallContext context)
        {
            var timestampValue = new DateTime((int)request.TimestampValue.Year, (int)request.TimestampValue.Month, (int)request.TimestampValue.Day, (int)request.TimestampValue.Hour, (int)request.TimestampValue.Minute, (int)request.TimestampValue.Second);

            return Task.FromResult(new SetTimestampValue_Responses { ReceivedValue = new SiLAFramework.String { Value = $"Received Timestamp value: {timestampValue}" } });
        }

        public override Task<Get_TimestampValue_Responses> Get_TimestampValue(Get_TimestampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_TimestampValue_Responses { TimestampValue = new SiLAFramework.Timestamp { Year = 2018, Month = 8, Day = 24, Hour = 12, Minute = 34, Second = 56, Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 } } });
        }

        #endregion

        #region AnyType

        public override Task<SetAnyTypeValue_Responses> SetAnyTypeValue(SetAnyTypeValue_Parameters request, ServerCallContext context)
        {
            try
            {
                // deserialize the type information
                var dataType = AnyType.ExtractAnyType(request.AnyTypeValue.Type);

                if (dataType.Item is BasicType)
                {
                    string payload = string.Empty;
                    switch ((BasicType)dataType.Item)
                    {
                        case BasicType.String:
                            payload = SiLAFramework.String.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Integer:
                            payload = SiLAFramework.Integer.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Real:
                            payload = SiLAFramework.Real.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Boolean:
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Binary:
                            var binary = SiLAFramework.Binary.Parser.ParseFrom(request.AnyTypeValue.Payload);
                            if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                            {
                                payload = SiLAFramework.Boolean.Parser.ParseFrom(request.AnyTypeValue.Payload).Value.ToString();
                                break;
                            }

                            //logger.Error($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                            break;
                        case BasicType.Date:
                            payload = SiLAFramework.Date.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Time:
                            payload = SiLAFramework.Time.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Timestamp:
                            payload = SiLAFramework.Timestamp.Parser.ParseFrom(request.AnyTypeValue.Payload).ToString();
                            break;
                        default:
                            throw new Exception($"'{dataType.Item.ToString()}' is not a valid AnyType type");
                    }

                    return Task.FromResult(new SetAnyTypeValue_Responses
                    {
                        ReceivedAnyType = new SiLAFramework.String { Value = ((BasicType)dataType.Item).ToString() },
                        ReceivedPayload = new SiLAFramework.String { Value = payload }
                    });
                }
            }
            catch (System.Xml.Schema.XmlSchemaValidationException ex)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    _silaFeature.GetFullyQualifiedCommandParameterIdentifier("SetAnyTypeValue", "AnyTypeValue"),
                    $"Given type description '{request.AnyTypeValue.Type}' is not matching the schema AnyTypeDataType.xsd: {ex.Message}"));
            }
            catch (Exception ex)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Exception in command SetAnyTypeValue: " + ex.Message));
            }

            return base.SetAnyTypeValue(request, context);
        }

        public override Task<Get_AnyTypeStringValue_Responses> Get_AnyTypeStringValue(Get_AnyTypeStringValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeStringValue_Responses
            {
                AnyTypeStringValue = AnyType.CreateAnyTypeObject("SiLA_Any_type_of_String_type")
            });
        }

        public override Task<Get_AnyTypeIntegerValue_Responses> Get_AnyTypeIntegerValue(Get_AnyTypeIntegerValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeIntegerValue_Responses
            {
                AnyTypeIntegerValue = AnyType.CreateAnyTypeObject(5124)
            });
        }

        public override Task<Get_AnyTypeRealValue_Responses> Get_AnyTypeRealValue(Get_AnyTypeRealValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeRealValue_Responses
            {
                AnyTypeRealValue = AnyType.CreateAnyTypeObject(3.1415926)
            });
        }

        public override Task<Get_AnyTypeBooleanValue_Responses> Get_AnyTypeBooleanValue(Get_AnyTypeBooleanValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeBooleanValue_Responses
            {
                AnyTypeBooleanValue = AnyType.CreateAnyTypeObject(true)
            });
        }

        public override Task<Get_AnyTypeBinaryValue_Responses> Get_AnyTypeBinaryValue(Get_AnyTypeBinaryValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeBinaryValue_Responses
            {
                AnyTypeBinaryValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Binary
                {
                    Value = ByteString.CopyFromUtf8("SiLA_Any_type_of_Binary_type")
                })
            });
        }

        public override Task<Get_AnyTypeDateValue_Responses> Get_AnyTypeDateValue(Get_AnyTypeDateValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeDateValue_Responses
            {
                AnyTypeDateValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Date
                {
                    Year = 2021,
                    Month = 7,
                    Day = 28,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeTimeValue_Responses> Get_AnyTypeTimeValue(Get_AnyTypeTimeValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeTimeValue_Responses
            {
                AnyTypeTimeValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Time
                {
                    Hour = 12,
                    Minute = 34,
                    Second = 56,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeTimestampValue_Responses> Get_AnyTypeTimestampValue(Get_AnyTypeTimestampValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_AnyTypeTimestampValue_Responses
            {
                AnyTypeTimestampValue = AnyType.CreateAnyTypeObject(new SiLAFramework.Timestamp
                {
                    Year = 2021,
                    Month = 7,
                    Day = 28,
                    Hour = 12,
                    Minute = 34,
                    Second = 56,
                    Timezone = new SiLAFramework.Timezone { Hours = 2, Minutes = 0 }
                })
            });
        }

        public override Task<Get_AnyTypeListValue_Responses> Get_AnyTypeListValue(Get_AnyTypeListValue_Parameters request, ServerCallContext context)
        {
            var list = new Google.Protobuf.Collections.RepeatedField<SiLAFramework.String>
            {
                new SiLAFramework.String {Value = "SiLA 2"},
                new SiLAFramework.String {Value = "Any"},
                new SiLAFramework.String {Value = "Type"},
                new SiLAFramework.String {Value = "String"},
                new SiLAFramework.String {Value = "List"}
            };

            FieldCodec<SiLAFramework.String> codec = FieldCodec.ForMessage(10, SiLAFramework.String.Parser);
            byte[] bytes = new byte[list.CalculateSize(codec)];
            CodedOutputStream cos = new CodedOutputStream(bytes);
            list.WriteTo(cos, codec);

            // serialize type
            string typeStringXML;
            var value = new DataTypeType { Item = new ListType { DataType = new DataTypeType { Item = BasicType.String } } };
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(value.GetType());
            using (System.IO.StringWriter textWriter = new System.IO.StringWriter())
            {
                xmlSerializer.Serialize(textWriter, value);
                typeStringXML = textWriter.ToString();
            }

            return Task.FromResult(new Get_AnyTypeListValue_Responses
            {
                AnyTypeListValue = new SiLAFramework.Any
                {
                    Type = typeStringXML,
                    Payload = ByteString.CopyFrom(bytes)
                }
            });
        }

        public class Structure
        {
            public SiLAFramework.String StringTypeValue { get; set; }
            public SiLAFramework.Integer IntegerTypeValue { get; set; }
            public SiLAFramework.Date DateTypeValue { get; set; }
        }

        public override Task<Get_AnyTypeStructureValue_Responses> Get_AnyTypeStructureValue(Get_AnyTypeStructureValue_Parameters request, ServerCallContext context)
        {
            // serialize type
            string typeStringXML;
            var structureType = new DataTypeType
            {
                Item = new StructureType
                {
                    Element = new SiLAElement[]
                    {
                        new SiLAElement {Identifier = "StructElementString", DisplayName = "Struct Element String", Description = "A String type structure element", DataType = new DataTypeType {Item = BasicType.String}},
                        new SiLAElement {Identifier = "StructElementInteger", DisplayName = "Struct Element Integer", Description = "An Integer type structure element", DataType = new DataTypeType {Item = BasicType.Integer}},
                        new SiLAElement {Identifier = "StructElementDate", DisplayName = "Struct Element Date", Description = "A Date type structure element", DataType = new DataTypeType {Item = BasicType.Date}}
                    }
                }
            };
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(structureType.GetType());
            using (System.IO.StringWriter textWriter = new System.IO.StringWriter())
            {
                xmlSerializer.Serialize(textWriter, structureType);
                typeStringXML = textWriter.ToString();
            }

            var structureValue = new Structure
            {
                StringTypeValue = new SiLAFramework.String { Value = "My String value" },
                IntegerTypeValue = new SiLAFramework.Integer { Value = 83737665 },
                DateTypeValue = new SiLAFramework.Date { Year = 2021, Month = 07, Day = 30, Timezone = new SiLAFramework.Timezone { Hours = 2 } }
            };



            var mapField = new Google.Protobuf.Collections.MapField<string, Google.Protobuf.WellKnownTypes.Value>();

            var structure = new Google.Protobuf.WellKnownTypes.Struct();
            structure.Fields.Add("StructElementString", new Google.Protobuf.WellKnownTypes.Value());


            byte[] bytes = new byte[structure.CalculateSize()];
            CodedOutputStream cos = new CodedOutputStream(bytes);
            structure.WriteTo(cos);

            return Task.FromResult(new Get_AnyTypeStructureValue_Responses
            {
                AnyTypeStructureValue = new SiLAFramework.Any
                {
                    Type = typeStringXML,
                    Payload = ByteString.CopyFrom(bytes)
                }
            });
        }

        #endregion

        #region Mixed values

        public override Task<SetSeveralValues_Responses> SetSeveralValues(SetSeveralValues_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received values: ");

            sb.Append($"\n\tString value: '{request.StringValue.Value}'");
            sb.Append($"\n\tInteger value: {request.IntegerValue.Value}");
            sb.Append($"\n\tReal value: {request.RealValue.Value}");
            sb.Append($"\n\tBoolean value: {request.BooleanValue.Value}");
            var dateValue = new DateTime((int)request.DateValue.Year, (int)request.DateValue.Month, (int)request.DateValue.Day);
            sb.Append($"\n\tDate value: {dateValue.Date}");
            var timeValue = new TimeSpan((int)request.TimeValue.Hour, (int)request.TimeValue.Minute, (int)request.TimeValue.Second);
            sb.Append($"\n\tTime value: {timeValue}");
            var timestampValue = new DateTime((int)request.TimeStampValue.Year, (int)request.TimeStampValue.Month, (int)request.TimeStampValue.Day, (int)request.TimeStampValue.Hour, (int)request.TimeStampValue.Minute, (int)request.TimeStampValue.Second);
            sb.Append($"\n\tTimeStamp value: {timestampValue}");

            return Task.FromResult(new SetSeveralValues_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.ToString() } });
        }

        public override Task<GetSeveralValues_Responses> GetSeveralValues(GetSeveralValues_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new GetSeveralValues_Responses
            {
                StringValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" },
                IntegerValue = new SiLAFramework.Integer { Value = 5124 },
                RealValue = new SiLAFramework.Real { Value = 3.1415926 },
                BooleanValue = new SiLAFramework.Boolean { Value = true },
                DateValue = new SiLAFramework.Date { Year = 2018, Month = 8, Day = 24 },
                TimeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56 },
                TimeStampValue = new SiLAFramework.Timestamp { Year = 2018, Month = 8, Day = 24, Hour = 12, Minute = 34, Second = 56 },
            });
        }

        #endregion

        #region Structure

        public override Task<SetStructureValue_Responses> SetStructureValue(SetStructureValue_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received structure element values:");

            sb.Append($"\n\tString type value: '{request.StructureValue.MyStructure.StringTypeValue.Value}'");
            sb.Append($"\n\tInteger type value: {request.StructureValue.MyStructure.IntegerTypeValue.Value}");
            sb.Append($"\n\tReal type value: {request.StructureValue.MyStructure.RealTypeValue.Value}");
            sb.Append($"\n\tBoolean type value: {request.StructureValue.MyStructure.BooleanTypeValue.Value}");
            sb.Append($"\n\tBinary type value: {request.StructureValue.MyStructure.BinaryTypeValue.Value.ToStringUtf8()}");
            var dateValue = new DateTime(
                (int)request.StructureValue.MyStructure.DateTypeValue.Year,
                (int)request.StructureValue.MyStructure.DateTypeValue.Month,
                (int)request.StructureValue.MyStructure.DateTypeValue.Day);
            sb.Append($"\n\tDate type value: {dateValue.Date}");
            var timeValue = new TimeSpan(
                (int)request.StructureValue.MyStructure.TimeTypeValue.Hour,
                (int)request.StructureValue.MyStructure.TimeTypeValue.Minute,
                (int)request.StructureValue.MyStructure.TimeTypeValue.Second);
            sb.Append($"\n\tTime type value: {timeValue}");
            var timestampValue = new DateTime(
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Year,
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Month,
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Day,
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Hour,
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Minute,
                (int)request.StructureValue.MyStructure.TimestampTypeValue.Second);
            sb.Append($"\n\tTimestamp type value: {timestampValue}");

            var dataType = AnyType.ExtractAnyType(request.StructureValue.MyStructure.AnyTypeValue.Type);
            if (dataType.Item is BasicType)
            {
                string payload = string.Empty;
                switch ((BasicType)dataType.Item)
                {
                    case BasicType.String:
                        payload = SiLAFramework.String.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Integer:
                        payload = SiLAFramework.Integer.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Real:
                        payload = SiLAFramework.Real.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Boolean:
                        payload = SiLAFramework.Boolean.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                        break;
                    case BasicType.Binary:
                        var binary = SiLAFramework.Binary.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload);
                        if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                        {
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        }

                        //logger.Error($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                        break;
                    case BasicType.Date:
                        payload = SiLAFramework.Date.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).ToString();
                        break;
                    case BasicType.Time:
                        payload = SiLAFramework.Time.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).ToString();
                        break;
                    case BasicType.Timestamp:
                        payload = SiLAFramework.Timestamp.Parser.ParseFrom(request.StructureValue.MyStructure.AnyTypeValue.Payload).ToString();
                        break;
                    default:
                        throw new Exception($"'{dataType.Item}' is not a valid AnyType type");
                }

                sb.Append($"\n\tAny type value: {(BasicType)dataType.Item} type {payload}");
            }

            return Task.FromResult(new SetStructureValue_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.ToString() } });
        }

        public override Task<Get_StructureValue_Responses> Get_StructureValue(Get_StructureValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StructureValue_Responses
            {
                StructureValue = new DataType_MyStructure
                {
                    MyStructure = new DataType_MyStructure.Types.MyStructure_Struct
                    {
                        StringTypeValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" },
                        IntegerTypeValue = new SiLAFramework.Integer { Value = 5124 },
                        RealTypeValue = new SiLAFramework.Real { Value = 3.1415926 },
                        BooleanTypeValue = new SiLAFramework.Boolean { Value = true },
                        BinaryTypeValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") },
                        DateTypeValue = new SiLAFramework.Date { Year = 2019, Month = 9, Day = 25 },
                        TimeTypeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56 },
                        TimestampTypeValue = new SiLAFramework.Timestamp { Year = 2019, Month = 9, Day = 25, Hour = 12, Minute = 34, Second = 56 },
                        AnyTypeValue = AnyType.CreateAnyTypeObject("SiLA2_Any_Type_String_Value")
                    }
                }
            });
        }

        public override Task<SetDeepStructureValue_Responses> SetDeepStructureValue(SetDeepStructureValue_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received outer structure element values:");

            sb.Append($"\n\tOuter String type value: '{request.DeepStructureValue.DeepStructure.OuterStringTypeValue.Value}'");
            sb.Append($"\n\tOuter Integer type value: {request.DeepStructureValue.DeepStructure.OuterIntegerTypeValue.Value}");
            sb.Append("\n\tMiddle structure element values:");
            sb.Append($"\n\t\tMiddle String type value: '{request.DeepStructureValue.DeepStructure.MiddleStructure.MiddleStringTypeValue.Value}'");
            sb.Append($"\n\t\tMiddle Integer type value: {request.DeepStructureValue.DeepStructure.MiddleStructure.MiddleIntegerTypeValue.Value}");
            sb.Append("\n\t\tInner structure element values:");
            sb.Append($"\n\t\t\tInner String type value: '{request.DeepStructureValue.DeepStructure.MiddleStructure.InnerStructure.InnerStringTypeValue.Value}'");
            sb.Append($"\n\t\t\tInner Integer type value: {request.DeepStructureValue.DeepStructure.MiddleStructure.InnerStructure.InnerIntegerTypeValue.Value}");

            return Task.FromResult(new SetDeepStructureValue_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.ToString() } });
        }

        public override Task<Get_DeepStructureValue_Responses> Get_DeepStructureValue(Get_DeepStructureValue_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_DeepStructureValue_Responses
            {
                DeepStructureValue = new DataType_DeepStructure
                {
                    DeepStructure = new DataType_DeepStructure.Types.DeepStructure_Struct
                    {
                        OuterStringTypeValue = new SiLAFramework.String { Value = "Outer_Test_String" },
                        OuterIntegerTypeValue = new SiLAFramework.Integer { Value = 1111 },
                        MiddleStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct
                        {
                            MiddleStringTypeValue = new SiLAFramework.String { Value = "Middle_Test_String" },
                            MiddleIntegerTypeValue = new SiLAFramework.Integer { Value = 2222 },
                            InnerStructure = new DataType_DeepStructure.Types.DeepStructure_Struct.Types.MiddleStructure_Struct.Types.InnerStructure_Struct
                            {
                                InnerStringTypeValue = new SiLAFramework.String { Value = "Inner_Test_String" },
                                InnerIntegerTypeValue = new SiLAFramework.Integer { Value = 3333 }
                            }
                        }
                    }
                }
            });
        }

        #endregion

        #region List

        public override Task<SetStringList_Responses> SetStringList(SetStringList_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received String values: ");
            foreach (var stringValue in request.StringList)
            {
                sb.Append($"'{stringValue.Value}', ");
            }

            return Task.FromResult(new SetStringList_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.Remove(sb.Length - 2, 2).ToString() } });
        }

        public override Task<Get_StringList_Responses> Get_StringList(Get_StringList_Parameters request, ServerCallContext context)
        {
            var response = new Get_StringList_Responses
            {
                StringList =
                        {
                            new SiLAFramework.String{ Value = "SiLA 2" },
                            new SiLAFramework.String{ Value = "is" },
                            new SiLAFramework.String{ Value = "great" }
                        }
            };

            return Task.FromResult(response);
        }

        public override Task<SetIntegerList_Responses> SetIntegerList(SetIntegerList_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received Integer values: ");
            foreach (var intValue in request.IntegerList)
            {
                sb.Append($"{intValue.Value}, ");
            }

            return Task.FromResult(new SetIntegerList_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.Remove(sb.Length - 2, 2).ToString() } });
        }

        public override Task<Get_IntegerList_Responses> Get_IntegerList(Get_IntegerList_Parameters request, ServerCallContext context)
        {
            var response = new Get_IntegerList_Responses
            {
                IntegerList =
                        {
                            new SiLAFramework.Integer{ Value = 1 },
                            new SiLAFramework.Integer{ Value = 2 },
                            new SiLAFramework.Integer{ Value = 3 }
                        }
            };

            return Task.FromResult(response);
        }

        public override Task<SetStructureList_Responses> SetStructureList(SetStructureList_Parameters request, ServerCallContext context)
        {
            var sb = new StringBuilder("Received structures: \n");
            foreach (var structureValue in request.StructureList)
            {
                sb.AppendLine("{");

                sb.AppendLine($"  String type value: '{structureValue.MyStructure.StringTypeValue.Value}'");
                sb.AppendLine($"  Integer type value: {structureValue.MyStructure.IntegerTypeValue.Value}");
                sb.AppendLine($"  Real type value: {structureValue.MyStructure.RealTypeValue.Value}");
                sb.AppendLine($"  Boolean type value: {structureValue.MyStructure.BooleanTypeValue.Value}");
                sb.AppendLine($"  Binary type value: {structureValue.MyStructure.BinaryTypeValue.Value.ToStringUtf8()}");
                var dateValue = new DateTime(
                    (int)structureValue.MyStructure.DateTypeValue.Year,
                    (int)structureValue.MyStructure.DateTypeValue.Month,
                    (int)structureValue.MyStructure.DateTypeValue.Day);
                sb.AppendLine($"  Date type value: {dateValue.Date}");
                var timeValue = new TimeSpan(
                    (int)structureValue.MyStructure.TimeTypeValue.Hour,
                    (int)structureValue.MyStructure.TimeTypeValue.Minute,
                    (int)structureValue.MyStructure.TimeTypeValue.Second);
                sb.AppendLine($"  Time type value: {timeValue}");
                var timestampValue = new DateTime(
                    (int)structureValue.MyStructure.TimestampTypeValue.Year,
                    (int)structureValue.MyStructure.TimestampTypeValue.Month,
                    (int)structureValue.MyStructure.TimestampTypeValue.Day,
                    (int)structureValue.MyStructure.TimestampTypeValue.Hour,
                    (int)structureValue.MyStructure.TimestampTypeValue.Minute,
                    (int)structureValue.MyStructure.TimestampTypeValue.Second);
                sb.AppendLine($" Timestamp type value: {timestampValue}");

                var dataType = AnyType.ExtractAnyType(structureValue.MyStructure.AnyTypeValue.Type);
                if (dataType.Item is BasicType)
                {
                    string payload = string.Empty;
                    switch ((BasicType)dataType.Item)
                    {
                        case BasicType.String:
                            payload = SiLAFramework.String.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Integer:
                            payload = SiLAFramework.Integer.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Real:
                            payload = SiLAFramework.Real.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Boolean:
                            payload = SiLAFramework.Boolean.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                            break;
                        case BasicType.Binary:
                            var binary = SiLAFramework.Binary.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload);
                            if (binary.UnionCase == SiLAFramework.Binary.UnionOneofCase.Value)
                            {
                                payload = SiLAFramework.Boolean.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).Value.ToString();
                                break;
                            }

                            //logger.Error($"Response contains the wrong union type '{binary.UnionCase}' (expected: '{SiLAFramework.Binary.UnionOneofCase.Value}')");
                            break;
                        case BasicType.Date:
                            payload = SiLAFramework.Date.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Time:
                            payload = SiLAFramework.Time.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).ToString();
                            break;
                        case BasicType.Timestamp:
                            payload = SiLAFramework.Timestamp.Parser.ParseFrom(structureValue.MyStructure.AnyTypeValue.Payload).ToString();
                            break;
                        default:
                            throw new Exception($"'{dataType.Item}' is not a valid AnyType type");
                    }

                    sb.AppendLine($"  Any type value: {(BasicType)dataType.Item} type {payload}");

                    sb.AppendLine("}");
                }
            }

            return Task.FromResult(new SetStructureList_Responses { ReceivedValues = new SiLAFramework.String { Value = sb.ToString() } });
        }

        public override Task<Get_StructureList_Responses> Get_StructureList(Get_StructureList_Parameters request, ServerCallContext context)
        {
            var response = new Get_StructureList_Responses();

            const string stringTypeValue = "SiLA2_Test_String_Value_";
            const string binaryStringTypeValue = "Binary_String_Value_";
            const int integerTypeValue = 5124;
            const double realTypeValue = 3.1415926;
            DateTime dateTimeValue = new DateTime(2019, 9, 25, 12, 34, 56);
            const string anyTypeStringValue = "Any_Type_String_Value_";

            for (uint i = 0; i < 3; i++)
            {
                response.StructureList.Add(new DataType_MyStructure
                {
                    MyStructure = new DataType_MyStructure.Types.MyStructure_Struct
                    {
                        StringTypeValue = new SiLAFramework.String { Value = stringTypeValue + (i + 1) },
                        IntegerTypeValue = new SiLAFramework.Integer { Value = integerTypeValue + i },
                        RealTypeValue = new SiLAFramework.Real { Value = realTypeValue + i },
                        BooleanTypeValue = new SiLAFramework.Boolean { Value = i % 2 == 0 },
                        BinaryTypeValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8(binaryStringTypeValue + (i + 1)) },
                        DateTypeValue = new SiLAFramework.Date
                        {
                            Year = (uint)dateTimeValue.Year + i,
                            Month = (uint)dateTimeValue.Month + i,
                            Day = (uint)dateTimeValue.Day + i
                        },
                        TimeTypeValue = new SiLAFramework.Time
                        {
                            Hour = (uint)dateTimeValue.Hour + i,
                            Minute = (uint)dateTimeValue.Minute + i,
                            Second = (uint)dateTimeValue.Second + i
                        },
                        TimestampTypeValue = new SiLAFramework.Timestamp
                        {
                            Year = (uint)dateTimeValue.Year + i,
                            Month = (uint)dateTimeValue.Month + i,
                            Day = (uint)dateTimeValue.Day + i,
                            Hour = (uint)dateTimeValue.Hour + i,
                            Minute = (uint)dateTimeValue.Minute + i,
                            Second = (uint)dateTimeValue.Second + i
                        },
                        AnyTypeValue = AnyType.CreateAnyTypeObject(anyTypeStringValue + (i + 1))
                    }
                });
            }

            return Task.FromResult(response);
        }

        #endregion

        #endregion
    }
}