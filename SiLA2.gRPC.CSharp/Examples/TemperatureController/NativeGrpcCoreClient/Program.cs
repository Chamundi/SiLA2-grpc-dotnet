﻿using Grpc.Core;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;
using System;
using System.IO;
using Sila2.Org.Silastandard.Core.Silaservice.V1;

namespace NativeGrpcCoreClient.App
{
    class Program
    {
        const string HOST = "localhost";

        static void Main(string[] args)
        {
#if DEBUG
            const int SECURE_PORT = 5001; // Please adjust Port if you want to connect to another SiLA2-Server than SiLA2.gRPC.Temperature.Service in Test Scenario
            //const int INSECURE_PORT = 5000;
#else
            const int SECURE_PORT = 13742;
#endif

            try
            {
                Console.WriteLine($"{Environment.NewLine}Sending Server-Name-Request SiLA2 Server by Native gRPC-Client...");
                GetServerResponse(SECURE_PORT, new SslCredentials(File.ReadAllText("SiLA2DevExampleSelfCert2.pem")), "Sending secure (https) Server-Name-Request SiLA2 Server by Native gRPC-Client ");
                //GetServerResponse(INSECURE_PORT, ChannelCredentials.Insecure, "Sending unencrypted (http) Server-Name-Request SiLA2 Server by Native gRPC-Client by http ");
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        private static void GetServerResponse(int port, ChannelCredentials sslCredentials, string requestText)
        {
            var channel = new Channel($"{HOST}:{port}", sslCredentials);
            Console.WriteLine($"{Environment.NewLine}{requestText} ({channel.Target})");
            var silaService = new SiLAServiceClient(channel);
            var response = silaService.Get_ServerName(new Get_ServerName_Parameters());
            Console.WriteLine($"{Environment.NewLine}Response from SiLA2 Server : {response.ServerName}");
        }
    }
}
