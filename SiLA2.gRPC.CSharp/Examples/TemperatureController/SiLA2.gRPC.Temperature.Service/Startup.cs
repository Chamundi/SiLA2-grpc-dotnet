﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SiLA2.Commands;
using SiLA2.Frontend.Razor.Services;
using SiLA2.Frontend.Razor.Services.UserManagement;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Database;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using SiLA2.Utils;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Reflection;
using TemperatureController.Features.Database;
using TemperatureController.Features.Services;

namespace SiLA2.gRPC.Temperature.Server.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(x => x.EnableDetailedErrors = true);
            services.AddGrpcReflection();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddDbContext<IDbUserContext, UserDbContext>(x => x.UseSqlite(_configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<IDbTemperatureProfileContext, TemperatureDbContext>(x => x.UseSqlite(_configuration.GetConnectionString("TemperatureServiceConnection")));
            services.AddScoped<IRepository<TemperatureProfile>, TemperatureProfileRepository>();
            services.AddScoped<ITemperatureProfileService, TemperatureProfileService>();
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddSingleton<IGrpcServerChannelProvider, GrpcServerChannelProvider>();
            services.AddScoped<IGrpcClientProvider, GrpcClientProvider>();
            services.AddScoped<IFeatureMapper, FeatureMapper>();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<IThermostatSimulator, ThermostatSimulator>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<TemperatureControllerService>();
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: TemperatureControllerService _ is injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, TemperatureControllerService _, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<LockControllerService>();
                endpoints.MapGrpcService<SiLABinaryDownloadService>();
                endpoints.MapGrpcService<SiLABinaryUploadService>();
                endpoints.MapGrpcService<TemperatureControllerService>();

                endpoints.MapGrpcReflectionService();

                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                endpoints.MapControllerRoute("Fallback",
                          "{controller}/{action}/{id?}",
                          new { controller = "App", action = "Index" });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
