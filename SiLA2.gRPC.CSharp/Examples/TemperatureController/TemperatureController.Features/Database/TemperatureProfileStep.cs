﻿using SiLA2.Server.Database.Domain;

namespace TemperatureController.Features.Database
{
    public class TemperatureProfileStep : BaseEntity
    {
        public double TemperatureInCelsius { get; private set; }
        public int DurationInMilliseconds { get; private set; }

        public TemperatureProfileStep(double temperatureInCelsius, int durationInMilliseconds)
        {
            TemperatureInCelsius = temperatureInCelsius;
            DurationInMilliseconds = durationInMilliseconds;
        }
    }
}
