﻿using DbUp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SiLA2.Server.Database;
using SiLA2.Server.Database.Domain;
using System;
using System.Collections.Generic;
using System.Reflection;
using TemperatureController.Features.Database.Maps;

namespace TemperatureController.Features.Database
{
    public interface IDbTemperatureProfileContext : IDbContext { }

    public class TemperatureDbContext : DbContext, IDbTemperatureProfileContext
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<TemperatureDbContext> _logger;

        public IEnumerable<Type> DbMappingTypes => new[] { typeof(TemperatureProfileStepMap), typeof(TemperatureProfileMap) };

        public TemperatureDbContext(DbContextOptions<TemperatureDbContext> options, IConfiguration configuration, ILogger<TemperatureDbContext> logger) : base(options)
        {
            _configuration = configuration;
            _logger = logger;
            if (Database.EnsureCreated())
            {
                var deploymentResult = DeployChanges.To.SQLiteDatabase(_configuration["ConnectionStrings:TemperatureServiceConnection"]).WithScriptsEmbeddedInAssemblies(new[] { Assembly.GetAssembly(typeof(TemperatureDbContext)) }).Build().PerformUpgrade();
                _logger.LogInformation($"Database {nameof(TemperatureDbContext)} Deployment succeeded : {deploymentResult.Successful}");
            }
        }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var type in DbMappingTypes)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}
