﻿using Microsoft.EntityFrameworkCore;
using SiLA2.Server.Database;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace TemperatureController.Features.Database
{
    public class TemperatureProfileRepository : Repository<TemperatureProfile>
    {
        public TemperatureProfileRepository(IDbTemperatureProfileContext context) : base(context) {  }

        public override async Task<TemperatureProfile> GetById(object id)
        {
            return await Entities.Include(x => x.Temperatures).SingleOrDefaultAsync(x => x.Id == (Guid)id);
        }
        public override IQueryable<TemperatureProfile> Table => Entities.Include(x => x.Temperatures);
    }
}
