using NUnit.Framework;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.gRPC.Temperature.Server.App;
using SiLA2.Server.Tests;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;

namespace TemperatureControllerTests
{
    public class ServerTests
    {
        private const string EXPECTED_SERVER_NAME = "SiLA2 Temperature Test Server";
        private const string EXPECTED_SERVER_TYPE = "SiLA2 Temperature Server";
        private const string EXPECTED_SERVER_UUID = "19767601-AEBF-4553-A8AC-75865BA8A072";

        private SiLAServiceClient _siLAServiceClient;
        private List<string> _expectedImplementedFeatures = new List<string>
                                                            {
                                                                { "org.silastandard/core/SiLAService/v1" },
                                                                { "org.silastandard/core/LockController/v1" },
                                                                { "org.silastandard/core/ErrorRecoveryService/v1" },
                                                                { "org.silastandard/examples/TemperatureController/v1"} };

        [OneTimeSetUp]
        public void SetupOnce()
        {
            // System under Test
            _siLAServiceClient = new SiLAServiceClient(new TestServerFixture<Startup>().GrpcChannel);
        }

        [Test]
        public async Task Should_Get_ServerNameAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerNameAsync(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public void Should_Get_ServerName()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public async Task Should_Get_ServerUUIDAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerUUIDAsync(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value, Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Get_ServerUUID()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerUUID(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value, Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public async Task Should_Get_ServerTypeAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerTypeAsync(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public void Should_Get_ServerType()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerType(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public async Task Should_Get_ImplementedFeaturesAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test]
        public void Should_Get_ImplementedFeatures()
        {
            // Act
            var result = _siLAServiceClient.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }
    }
}