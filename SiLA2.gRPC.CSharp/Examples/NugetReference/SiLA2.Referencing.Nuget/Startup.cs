﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SiLA2.Commands;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.ReferencingNuget.Features.Services;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using SiLA2.Utils;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Reflection;

namespace SiLA2.ReferencingNuget.Server.App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(x => x.EnableDetailedErrors = true);
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<IThermostatSimulator, ThermostatSimulator>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddSingleton<IGrpcServerChannelProvider, GrpcServerChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<TemperatureControllerService>();
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(_configuration.GetSection("ServerConfig"), configFile);
        }

        // Note: TemperatureControllerService _ is injected for calling its constructor in which the SilaFeature is added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, TemperatureControllerService _, ILogger<Startup> logger)
        {
            logger.LogInformation(app.ApplicationServices.GetService<ServerInformation>().ToString());
            logger.LogInformation("Starting Server Announcement...");

            var sila2Server = app.ApplicationServices.GetService<ISiLA2Server>();
            var features = _configuration.GetSection("FeatureFiles").GetChildren();
            foreach (var featureFilePath in features)
            {
                sila2Server.ReadFeature(featureFilePath.Value);
            }
            sila2Server.Start();

            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<TemperatureControllerService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
