using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sila2.Examples.MockShaker;
using SiLA2.Commands;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using ShakerController.Features.Services;
using SiLA2.Frontend.Razor.Services;
using SiLA2.Server.Database;
using Microsoft.EntityFrameworkCore;
using SiLA2.Utils;
using SiLA2.Utils.Network;
using SiLA2.Utils.gRPC;
using SiLA2.Network;
using SiLA2.Frontend.Razor.Services.UserManagement;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using System;
using System.IO;
using System.Reflection;

namespace SiLA2.gRPC.Shaker.Server.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc(x => x.EnableDetailedErrors = true);
            services.AddGrpcReflection();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddDbContext<IDbUserContext, UserDbContext>(x => x.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IGrpcServerChannelProvider, GrpcServerChannelProvider>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IGrpcClientProvider, GrpcClientProvider>();
            services.AddScoped<IFeatureMapper, FeatureMapper>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<IShakerSimulator, ShakerSimulator>();
            services.AddSingleton<PlateHandlingService>();
            services.AddSingleton<ShakingService>();
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(Configuration.GetSection("ServerConfig"), configFile);
        }

        //Note: PlateHandlingService _1, ShakingService _2 are injected for calling their constructors in which the SilaFeature are added to the SiLA2.Server feature collection
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ISiLA2Server siLA2Server, ServerInformation serverInformation, PlateHandlingService _1, ShakingService _2, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<SiLAService>();
                endpoints.MapGrpcService<LockControllerService>();
                endpoints.MapGrpcService<SiLABinaryDownloadService>();
                endpoints.MapGrpcService<SiLABinaryUploadService>();
                endpoints.MapGrpcService<PlateHandlingService>();
                endpoints.MapGrpcService<ShakingService>();

                endpoints.MapGrpcReflectionService();

                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                endpoints.MapControllerRoute("Fallback",
                          "{controller}/{action}/{id?}",
                          new { controller = "App", action = "Index" });
            });

            logger.LogInformation($"{serverInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {GetType().Assembly.FullName}");
        }
    }
}
