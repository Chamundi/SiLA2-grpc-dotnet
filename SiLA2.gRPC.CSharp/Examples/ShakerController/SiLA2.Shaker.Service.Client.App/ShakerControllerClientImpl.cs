﻿namespace Sila2.Examples.MockShaker.ClientApp
{
    using System;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Grpc.Core;
    using SiLAFramework = Org.Silastandard;
    using Grpc.Net.Client;
    using Microsoft.Extensions.Logging;
    using SiLA2.Server.Utils;
    using Sila2.De.Equicon.Mixing.Shakingcontrol.V1;
    using Sila2.De.Equicon.Handling.Platehandlingcontrol.V1;

    public class ShakerControllerClientImpl
    {
        private readonly ILogger<ShakerControllerClientImpl> _logger;
        private readonly ShakingControl.ShakingControlClient _shakingClient;
        private readonly PlateHandlingControl.PlateHandlingControlClient _handlingClient;

        public ShakerControllerClientImpl(GrpcChannel channel, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ShakerControllerClientImpl>();
            _shakingClient = new ShakingControl.ShakingControlClient(channel);
            _handlingClient = new PlateHandlingControl.PlateHandlingControlClient(channel);
        }

        #region ShakingControl commands

        public async Task Shake(string duration, int speed)
        {
            var cmdId = _shakingClient.Shake(new Shake_Parameters { Duration = new SiLAFramework.String { Value = duration }, Speed = new SiLAFramework.Integer { Value = speed } }).CommandExecutionUUID;

            _logger.LogInformation($"Shake command started with speed: {speed} and duration: {duration} ...");

            // wait for progress has been finished
            try
            {
                using (var call = _shakingClient.Shake_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;

                    while (await responseStream.MoveNext())
                    {
                        Console.Write("\rProgress: {0:0.00}     Status: {1}", responseStream.Current.ProgressInfo.Value, responseStream.Current.CommandStatus);
                        if (responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                    Console.Write("\n");
                }
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }

            // get result
            try
            {
                var response = _shakingClient.Shake_Result(cmdId);
                _logger.LogInformation("Shake command response received: " + response);
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        #endregion

        #region Plate Handling commands

        public void OpenClamp()
        {
            _logger.LogInformation("Opening clamp");
            OpenClamp_Responses response;
            try
            {
                response = _handlingClient.OpenClamp(new OpenClamp_Parameters());
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        public void CloseClamp()
        {
            _logger.LogInformation("Closing clamp...");
            CloseClamp_Responses response;
            try
            {
                response = _handlingClient.CloseClamp(new CloseClamp_Parameters());
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }
        }

        public bool? IsClampOpen()
        {
            Get_CurrentClampState_Responses response = null;
            try
            {
                response = _handlingClient.Get_CurrentClampState(new Get_CurrentClampState_Parameters());
            }
            catch (RpcException e)
            {
                _logger.LogError(ErrorHandling.HandleException(e));
            }

            return response != null ? response.CurrentClampState.Value : null;
        }

        #endregion

        #region Helper methods

        public static object StringToObject(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                //TODO : BinaryFormatter is insecure -> alternative implementation
                return new BinaryFormatter().Deserialize(ms);
            }
        }
        #endregion

        public void StopShaking()
        {
            _shakingClient.StopShaking(new StopShaking_Parameters());
        }
    }
}