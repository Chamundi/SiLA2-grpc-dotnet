﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Sila2.Examples.MockShaker.ClientApp;
using SiLA2.Client;
using SiLA2.Server.Utils;
using SiLA2.Utils.gRPC;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Shaker.Service.Client.App
{


    class Program
    {
        private static IConfigurationRoot _configuration;

        private static void DisplayClampState(ShakerControllerClientImpl client)
        {
            var clampState = client.IsClampOpen();
            if (clampState.HasValue)
            {
                Console.WriteLine($"Clamp state = {(clampState.Value ? "open" : "closed")}");
            }
            else
            {
                Console.WriteLine("Clamp state is undefined");
            }
        }

        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            _configuration = configBuilder.Build();

            var clientSetup = new Configurator(_configuration);

            var loggerFactory = clientSetup.ServiceProvider.GetService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger<Program>();

            Console.WriteLine("Starting Server Discovery...");

            var serverMap = await clientSetup.SearchForServers();

            GrpcChannel channel;
            var serverType = "SiLA2 Shaker Server";
            var server = serverMap.Values.FirstOrDefault(x => x.Info.Type == serverType);
            if (server != null)
            {
                Console.WriteLine($"Connecting to {server}");
                channel = server.Channel;
            }
            else
            {
                var fqhn = _configuration["Connection:FQHN"];
                var port = int.Parse(_configuration["Connection:Port"]);
                Console.WriteLine($"No connection automatically discovered. Using Server-URI '{fqhn}:{port}' from appSettings.config");
                channel = await clientSetup.ServiceProvider.GetService<IGrpcChannelProvider>().GetChannel(fqhn, port, true);
            }

            try
            {
                Console.WriteLine("Trying to setup Client ...");
                // create the client
                var client = new ShakerControllerClientImpl(channel, loggerFactory);

                // test clamp handling
                DisplayClampState(client);
                client.OpenClamp();
                DisplayClampState(client);

                // try shaking with invalid duration parameter
                try
                {
                    client.Shake("7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // try shaking with opened clamp
                try
                {
                    client.Shake("PT7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // close clamp in order to shake
                client.CloseClamp();
                DisplayClampState(client);

                // shake for 7 seconds
                client.Shake("PT7S", 3000).Wait();

                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                logger.LogError($"{e}");
            }

            Console.ReadKey();
        }
    }
}
