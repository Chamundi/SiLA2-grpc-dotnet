﻿using Microsoft.Extensions.Logging;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Threading.Tasks;

namespace SiLA2.IPC.NetMQ
{
    public class ZeroMqServer : IZeroMqServer
    {
        private readonly IZeroMqServerConfig _zeroMqServerConfig;
        private readonly ILogger<ZeroMqServer> _logger;

        public ZeroMqServer(IZeroMqServerConfig zeroMqServerConfig, ILogger<ZeroMqServer> logger)
        {
            _zeroMqServerConfig = zeroMqServerConfig;
            _logger = logger;
        }

        public Task Run()
        {
            using (var server = new ResponseSocket())
            {
                server.Bind(_zeroMqServerConfig.ServerSocketAddress);
                while (true)
                {
                    try
                    {
                        var message = server.ReceiveMultipartMessage();
                        _logger.LogDebug($"Received Message: '{message}'");
                        server.SendFrame($"Message from Server : '{_zeroMqServerConfig.ProcessRequest(message)}'");
                    }
                    catch (Exception ex)
                    {
                        server.SendFrame($"Server Exception : '{ex}'");
                    }
                }
            }
        }

        public void SetRequestProssing(Func<NetMQMessage, string> requestProcessing)
        {
            _zeroMqServerConfig.ProcessRequest = requestProcessing;
        }
    }
}
