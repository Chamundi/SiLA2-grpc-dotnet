﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SiLA2.Server;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public interface IConfigurator
    {
        IServiceCollection Container { get; }
        IDictionary<Guid, ServerData> DiscoveredServers { get; }
        IServiceProvider ServiceProvider { get; }
        Task<IDictionary<Guid, ServerData>> SearchForServers();
    }
}