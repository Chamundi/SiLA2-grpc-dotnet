﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Network;
using SiLA2.Network.Discovery;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class Configurator : IConfigurator
    {
        private ILogger<Configurator> _logger;

        public IServiceCollection Container { get; private set; }

        public IServiceProvider ServiceProvider { get; }

        public IDictionary<Guid, ServerData> DiscoveredServers { get; private set; } = new Dictionary<Guid, ServerData>();

        public Configurator(IConfigurationRoot configuration)
        {
            SetupContainer(configuration);
            ServiceProvider = Container.BuildServiceProvider();

            _logger = ServiceProvider.GetService<ILogger<Configurator>>();
        }

        public async Task<IDictionary<Guid, ServerData>> SearchForServers()
        {
            var servers = await ServiceProvider.GetService<ISiLAServerDiscovery>().GetServers();

            foreach (var server in servers)
            {
                if (!DiscoveredServers.ContainsKey(server.Config.Uuid))
                {
                    DiscoveredServers.Add(server.Config.Uuid, server);
                }
                else
                {
                    DiscoveredServers[server.Config.Uuid] = server;
                }
                _logger.LogInformation($"Found {server.Config}");
            }

            return DiscoveredServers;
        }

        private void SetupContainer(IConfigurationRoot configuration)
        {
            Container = new ServiceCollection();
            Container.AddLogging(x => x.AddConsole());
            Container.AddSingleton<IConfiguration>(configuration);
            Container.AddSingleton<IServiceFinder, ServiceFinder>();
            Container.AddSingleton<INetworkService, NetworkService>();
            Container.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            Container.AddSingleton<IGrpcServerChannelProvider, GrpcServerChannelProvider>();
            Container.AddSingleton<ISiLAServerDiscovery, SiLAServerDiscovery>();
            Container.AddScoped<IServerDataProvider, ServerDataProvider>();
        }
    }
}