﻿using NUnit.Framework;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLA2.gRPC.Temperature.Server.Basic.App;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class SiLAServiceTests
    {
        // Arrange
        private const string EXPECTED_SERVER_NAME = "SiLA2 Temperature Basic Test Server";
        private const string EXPECTED_SERVER_TYPE = "SiLA2 Temperature Server";
        private const string EXPECTED_SERVER_UUID = "BDD8CCD6-D81E-4E84-A081-02585951112D";
        private const string EXPECTED_SERVER_DESCRIPTION = "SiLA2 Server with grpc-dotnet implementation. Basic example...";
        private const string EXPECTED_SERVER_VENDOR_URL = "https://sila2.gitlab.io/sila_base/";
        private const string EXPECTED_SERVER_VERSION = "v3.0 (grpc-dotnet implementation)";
        private const string EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART = "This Feature MUST be implemented by each SiLA Server.    It specifies Commands and Properties to discover the Features a SiLA Server implements as well as details    about the SiLA Server, like name, type, description, vendor and UUID.    Any interaction described in this feature MUST not affect the behaviour of any other Feature.";
        private const string EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART = "This Feature allows a SiLA Client to lock a SiLA Server for exclusive use, preventing other SiLA Clients";
        private const string EXPECTED_TEMPERATURE_CONTROLLER_FEATURE_DEFINITION_PART = "This is a simple example of a generic Feature for controlling and retrieving the temperature.";

        private SiLAServiceClient _siLAServiceClient;
        private List<string> _expectedImplementedFeatures = new List<string> 
                                                            { 
                                                                { "org.silastandard/core/SiLAService/v1" },
                                                                { "org.silastandard/core/LockController/v1" },
                                                                { "org.silastandard/core/ErrorRecoveryService/v1" },
                                                                { "org.silastandard/examples/TemperatureController/v1"} };
        [OneTimeSetUp]
        public void SetupOnce()
        {
            // System under Test
            _siLAServiceClient = new SiLAServiceClient(new TestServerFixture<Startup>().GrpcChannel);
        }

        [Test]
        public async Task Should_Get_ServerNameAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerNameAsync(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public void Should_Get_ServerName() 
        { 
            // Act
            var result = _siLAServiceClient.Get_ServerName(new Get_ServerName_Parameters());

            // Assert
            Assert.That(result.ServerName.Value, Is.EqualTo(EXPECTED_SERVER_NAME));
        }

        [Test]
        public async Task Should_Get_ServerUUIDAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerUUIDAsync(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value, Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [Test]
        public void Should_Get_ServerUUID()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerUUID(new Get_ServerUUID_Parameters());

            // Assert
            Assert.That(result.ServerUUID.Value, Is.EqualTo(EXPECTED_SERVER_UUID));
        }

        [TestCase("org.silastandard/core/SiLAService/v1", EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/LockController/v1", EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/examples/TemperatureController/v1", EXPECTED_TEMPERATURE_CONTROLLER_FEATURE_DEFINITION_PART)]
        public async Task Should_Get_FeatureDefinitionAsync(string featureDefinitionId, string resultStringPart)
        {
            // Arrange
            var featureDefinitionParameter = new GetFeatureDefinition_Parameters();
            var featureDefId = new Sila2.Org.Silastandard.String();
            featureDefId.Value = featureDefinitionId;
            featureDefinitionParameter.FeatureIdentifier = featureDefId;

            // Act
            var result = await _siLAServiceClient.GetFeatureDefinitionAsync(featureDefinitionParameter);

            // Assert
            Assert.That(result.FeatureDefinition.Value.IndexOf(resultStringPart)> -1);
        }

        [TestCase("org.silastandard/core/SiLAService/v1", EXPECTED_SILA_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/core/LockController/v1", EXPECTED_LOCKCONTROLLER_SERVICE_FEATURE_DEFINITION_PART)]
        [TestCase("org.silastandard/examples/TemperatureController/v1", EXPECTED_TEMPERATURE_CONTROLLER_FEATURE_DEFINITION_PART)]
        public void Should_Get_FeatureDefinition(string featureDefinitionId, string resultStringPart)
        {
            // Arrange
            var featureDefinitionParameter = new GetFeatureDefinition_Parameters();
            var featureDefId = new Sila2.Org.Silastandard.String { Value = featureDefinitionId };
            featureDefinitionParameter.FeatureIdentifier = featureDefId;

            // Act
            var result = _siLAServiceClient.GetFeatureDefinition(featureDefinitionParameter);

            // Assert
            Assert.That(result.FeatureDefinition.Value.IndexOf(resultStringPart) > -1);
        }

        [Test]
        public async Task Should_Get_ServerVersionAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerVersionAsync(new Get_ServerVersion_Parameters());

            // Assert
            Assert.That(result.ServerVersion.Value, Is.EqualTo(EXPECTED_SERVER_VERSION));
        }

        [Test]
        public void Should_Get_ServerVersion()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerVersion(new Get_ServerVersion_Parameters());

            // Assert
            Assert.That(result.ServerVersion.Value, Is.EqualTo(EXPECTED_SERVER_VERSION));
        }

        [Test]
        public async Task Should_Get_ServerVendorURLAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerVendorURLAsync(new Get_ServerVendorURL_Parameters());

            // Assert
            Assert.That(result.ServerVendorURL.Value, Is.EqualTo(EXPECTED_SERVER_VENDOR_URL));
        }

        [Test]
        public void Should_Get_ServerVendorURL()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters());

            // Assert
            Assert.That(result.ServerVendorURL.Value, Is.EqualTo(EXPECTED_SERVER_VENDOR_URL));
        }

        [Test]
        public async Task Should_Get_ServerDescriptionAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerDescriptionAsync(new Get_ServerDescription_Parameters());

            // Assert
            Assert.That(result.ServerDescription.Value, Is.EqualTo(EXPECTED_SERVER_DESCRIPTION));
        }

        [Test]
        public void Should_Get_ServerDescription()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerDescription(new Get_ServerDescription_Parameters());

            // Assert
            Assert.That(result.ServerDescription.Value, Is.EqualTo(EXPECTED_SERVER_DESCRIPTION));
        }

        [Test]
        public async Task Should_Get_ServerTypeAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ServerTypeAsync(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public void Should_Get_ServerType()
        {
            // Act
            var result = _siLAServiceClient.Get_ServerType(new Get_ServerType_Parameters());

            // Assert
            Assert.That(result.ServerType.Value, Is.EqualTo(EXPECTED_SERVER_TYPE));
        }

        [Test]
        public async Task Should_Get_ImplementedFeaturesAsync()
        {
            // Act
            var result = await _siLAServiceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach(var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }

        [Test]
        public void Should_Get_ImplementedFeatures()
        {
            // Act
            var result = _siLAServiceClient.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters());

            // Assert
            Assert.That(result.ImplementedFeatures.Count, Is.EqualTo(_expectedImplementedFeatures.Count));
            foreach (var feature in result.ImplementedFeatures)
            {
                Assert.That(_expectedImplementedFeatures.Contains(feature.Value));
            }
        }
    }
}
